from __future__ import print_function, unicode_literals

# Trivial dummy directory implementation for development.

def directory_url(value):
    return "http://directory.example/%s" % value

def directory_name(value):
    return value

def groups_for_user(username):
    return []

# We never raise this, but someone might test for it.
class DirectoryException(Exception):
    pass

