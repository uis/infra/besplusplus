from __future__ import print_function, unicode_literals

# Directory wrapper for University of Cambridge lookup service.

import re
from ibisclient import (
    createConnection, PersonMethods, GroupMethods, IbisException
    )

def lookup_canon(value):
    """
    Canonicalise a reference to lookup into a sequence of path components.
    If the argument contains a '/', just split it on slashes.  If there's
    no '/', try to guess whether it's a group ID (all-numeric) or a CRSID
    (otherwise) and assemble a suitable sequence.
    """
    parts = value.split('/')
    if len(parts) == 1:
        if re.match(r'^PWFAD\\', parts[0]):
            return ['person', 'crsid', parts[0][6:]]
        elif re.match(r'^\d*$', parts[0]):
            return ['group', parts[0]]
        else:
            return ['person', 'crsid', parts[0]]
    return parts

def directory_url(value):
    return "https://www.lookup.cam.ac.uk/" + '/'.join(lookup_canon(value))

def directory_name(value):
    try:
        c = createConnection()
        parts = lookup_canon(value)
        if parts[0] == 'person':
            pm = PersonMethods(c)
            person = pm.getPerson(parts[1], parts[2])
            if person:
                return person.visibleName
        if parts[0] == 'group':
            gm = GroupMethods(c)
            group = gm.getGroup(parts[1])
            if group:
                return group.title
    except Exception:
        pass
    return value

# This returns potential values of the <sysadmin> property that might
# refer to the current user.  It's used by MineView.
def groups_for_user(username):
    c = createConnection()
    pm = PersonMethods(c)
    person = pm.getPerson('crsid', username, fetch='all_groups')
    if person == None:
        return []
    else:
        return sum([[g.groupid, g.name] for g in person.groups], [])
    
# Alias for the top-level ibisclient exception class
DirectoryException = IbisException
