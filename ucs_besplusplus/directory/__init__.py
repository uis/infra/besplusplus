from __future__ import print_function, unicode_literals

# Load a working directory implementation.  Either ibisclient (for the
# University of Cambridge lookup service) or a dummy implementation.
# Other directory systems could potentially plug in here if necessary.

try:
    from .ibis import (directory_url, directory_name, groups_for_user,
                       DirectoryException)
except ImportError:
    from .dummy import (directory_url, directory_name, groups_for_user,
                        DirectoryException)
