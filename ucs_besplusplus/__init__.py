from __future__ import unicode_literals

# As a pre-1.7 application, Bes++ uses default_app_config.
# https://docs.djangoproject.com/en/2.0/ref/applications/

default_app_config = 'ucs_besplusplus.apps.BesPlusPlusConfig'
