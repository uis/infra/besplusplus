$(document).ready(function() {
    function deleteChanged(e) {
	var controls = $(e.target).parent().siblings(".campl-controls")
	if (e.target.checked) {
	    controls.addClass("besplusplus-will-delete");
	} else {
	    controls.removeClass("besplusplus-will-delete");
	}
    }
    $("input[type='checkbox']").change(deleteChanged);
});
