from __future__ import unicode_literals

from django.conf import settings
from django.core.exceptions import (ObjectDoesNotExist,
    MultipleObjectsReturned, ValidationError)
from django.urls import reverse
from django.db import models
from django.db.transaction import atomic
from django.dispatch import Signal, receiver
from uuid import uuid4, uuid5, NAMESPACE_DNS
from functools import partial, wraps
from itertools import chain
from socket import inet_pton, AF_INET, AF_INET6
from django.utils.encoding import python_2_unicode_compatible
from django.utils.six import string_types

def id_from_fqdn(fqdn):
    """
    Convert an FQDN into a resource ID.
    """
    id = "dns--" + fqdn.lower().replace("-", "--").replace(".", "-")
    if len(id) > 63:
        return str(uuid5(NAMESPACE_DNS, str(fqdn.lower())))
    return id

class ResourceManager(models.Manager):
    def get_id(self, id):
        """
        Return the resource identified by a particular ID, including
        cases where the ID is an alias.  Raise ObjectDoesNotExist if
        it's not there.  If there are multiple resources with an ID,
        return the one with the lexicographically earliest ID.
        """
        try:
            return self.filter(models.Q(id=id)|PQ(alias=id)).distinct()[0]
        except IndexError as e:
            # We'd like to report _which_ resource was missing.
            raise Resource.DoesNotExist(
                'Resource <%s> does not exist.' % (id,))
            
    @atomic
    def intern(self, id):
        """
        Like get_id() combined with get_or_create().  Except it doesn't
        return a tuple because that's just annoying.  If the requested
        "id" includes "--", it's treated as a foreign identifier and
        turned into an alias if a resource has to be created.
        """
        try:
            return self.get_id(id)
        except ObjectDoesNotExist:
            if '--' in id:
                r = self.create()
                r.addprop('alias', id)
                return r
            return self.create(id=id)
    def intern_fqdn(self, fqdn):
        return self.intern(id_from_fqdn(fqdn))

class LookupHelper(object):
    """
    This class contains a callable and exposes it as a mapping so that it
    can be invoked by a Django template.
    """
    def __init__(self, f):
        self.f = f
    def __getitem__(self, key):
        return self.f(key)
    __slots__ = ['f']

def lookupmethod(f):
    """
    This can be used as a decorator to turn a two-argument function into
    one that can also take one argument and return a mapping that
    encapsulates the function.  This means that it can turn a method into
    something that can be invoked with an argument by a Django template.
    """
    @wraps(f)
    def g(self, *args):
        if args:
            return f(self, *args)
        return LookupHelper(partial(f, self))
    return g

def struuid4():
    return str(uuid4())

@python_2_unicode_compatible
class Resource(models.Model):
    # Only one actual field -- the primary key.
    id = models.CharField(max_length=255, primary_key=True, default=struuid4)
    # Override the default manager.
    objects = ResourceManager()
    # Special methods called by Python and Django
    def __str__(self, recursing=None):
        return self.stringify()
    def stringify(self, recursing=None):
        # "recursing" protects agains unbounded recursion
        if recursing == None: recursing = set()
        if self.id in recursing: return '...'
        labels = self.prop('label')
        if labels == []:
            labels = self.prop('fqdn')
        if labels:
            parents = self.rprop('incorporates')
            if parents:
                return (parents[0].stringify(recursing | set([self.id])) +
                        ':' + labels[0])
            return labels[0]
        return "<%s>" % self.id
    def get_absolute_url(self):
        return reverse('resource', args=[str(self.id)])
    # Arrange that a resource compares equal to its ID.  This makes code
    # simpler, e.g. "if 'rack' in thing.fwd('type'):"
    def __eq__(self, other):
        if self.id == other: return True
        return super(Resource, self).__eq__(other)
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        return super(Resource, self).__hash__()
    # Generic methods for our use.
    def prop(self, predicate):
        return [p.object for p in
                self.properties_fwd.all().select_related() if p.predicate == predicate]
    def rprop(self, predicate):
        return [p.subject for p in
                self.properties_rev.all().select_related() if p.predicate == predicate]
    # These methods are designed for access from templates, hence the
    # '@lookupmethod' and slight munging of property names.
    @lookupmethod
    def fwd(self, key):
        return self.prop(key.replace('_', '-'))
    @lookupmethod
    def rev(self, key):
        return self.rprop(key.replace('_', '-'))
    # Methods to add and delete properties forward and backward.
    def canonicalise_source(self, source):
        if source == None:
            source = settings.BESPLUSPLUS_SELF_ID
        if not isinstance(source, Resource):
            return Resource.objects.get(pk=source)
        return source
    def addprop(self, predicate, object, source=None):
        if not isinstance(predicate, Resource):
            predicate = Resource.objects.get(pk=predicate)
        # Don't do the same for object, since it might legitimately be
        # a string.  Callers can pass in Resource("thing") if they need
        # to refer to an object resource by name.
        p = Property(subject=self, predicate=predicate,
                     source=self.canonicalise_source(source))
        p.object = object
        p.save()
    def addrprop(self, predicate, subject, source=None):
        if not isinstance(subject, Resource):
            subject = Resource.objects.get(pk=subject)
        if not isinstance(predicate, Resource):
            predicate = Resource.objects.get(pk=predicate)
        Property(subject=subject, predicate=predicate, object=self,
                 source=self.canonicalise_source(source)).save()
    def delprop(self, predicate, object, source=None):
        if not isinstance(predicate, Resource):
            predicate = Resource(pk=predicate)
        # Don't do the same for object, since it might legitimately be
        # a string.  Callers can pass in Resource("thing") if they need
        # to refer to an object resource by name.
        source = self.canonicalise_source(source)
        if isinstance(object, Resource):
            ps = self.properties_fwd.filter(predicate=predicate,
                                            object_res=object, source=source)
        else:
            ps = self.properties_fwd.filter(predicate=predicate,
                                            object_lit=object, source=source)
        ps.delete()
    def delrprop(self, predicate, subject, source=None):
        if not isinstance(predicate, Resource):
            predicate = Resource(predicate)
        self.properties_rev.get(predicate=predicate,
                                subject=subject,
                                source=self.canonicalise_source(source)
                                    ).delete()
    @atomic
    def setprop(self, predicate, objects, source=None):
        objects_res = set([ o for o in objects if isinstance(o, Resource) ])
        objects_lit = set([ o for o in objects if not isinstance(o, Resource) ])
        source = self.canonicalise_source(source)
        ps = self.properties_fwd.filter(predicate=predicate, source=source)
        for o in objects_res:
            ps = ps.exclude(object_res=o)
        for o in objects_lit:
            ps = ps.exclude(object_lit=o)
        # Anything remaining is not in the new list and hence can be deleted.
        ps.delete()
        for p in self.properties_fwd.filter(predicate=predicate):
            if p.object_res:
                objects_res.difference_update([p.object_res])
            else:
                objects_lit.difference_update([p.object_lit])
        for o in list(objects_res) + list(objects_lit):
            self.addprop(predicate, o, source)
    def setrprop(self, predicate, subjects, source=None):
        subjects = set(subjects)
        source = self.canonicalise_source(source)
        ps = self.properties_rev.filter(predicate=predicate, source=source)
        for s in subjects:
            ps = ps.exclude(subject=s)
        # Anything remaining is not in the new list and hence can be deleted.
        ps.delete()
        for p in self.properties_rev.filter(predicate=predicate):
            subjects.difference_update([p.subject])
        for s in subjects:
            self.addrprop(predicate, s, source=source)
    def listprop(self):
        return set([p.predicate for p in self.properties_fwd.all()])
    def listrprop(self):
        return set([p.predicate for p in self.properties_rev.all()])
                            
    def search_transitively(self, prop, p, seen=None):
        """
        Transitively follow "prop" properties of this resource, finding the
        first one(s) that satisfy the callable "p".
        """
        # In future, this could use a recursive query, but SQLite can't do that.
        # WITH RECURSIVE subclasses (superclass, subclass) AS (
        #   SELECT id AS superclass, id AS subclass FROM ucs_besplusplus_resource
        #  UNION
        #   SELECT object_res_id AS superclass, subclass
        #   FROM subclasses sc, ucs_besplusplus_property p
        #   WHERE sc.superclass = p.subject_id AND p.predicate_id = ?
        #    AND p.object_res_id IS NOT NULL)
        # SELECT * FROM subclasses;
        if seen == None: seen = set()
        seen.add(self)
        if p(self):
            return [self]
        return chain(*[x.search_transitively(prop, p, seen)
                       for x in self.prop(prop) if isinstance(x, Resource) and
                                                   x not in seen])

    def merge(self, others):
        """
        Merge all the resources in 'others' into this one and fix up
        references.  Add appropriate 'alias' properties to this resource.
        """
        for r in others:
            self.merge1(r)
    @atomic
    def merge1(self, other):
        # I am already myself -- no need to merge anything
        if self == other: return
        for p in other.properties_fwd.all():
            p.subject = self
            p.save()
        for p in other.properties_rev.all():
            p.object = self
            p.save()
        for p in other.properties_sideways.all():
            p.predicate = self
            p.save()
        self.addprop('alias', other.id)
        other.delete()
    def has_type(self, qt):
        return qt in self.prop('type')
    # Specialised methods for particular types of object.
    def ipv4_subnet_contains(self, ipaddr):
        """
        Indicate whether this <ipv4-subnet> contains a particular
        IPv4 address.
        """
        base = inet_pton(AF_INET, self.prop('ipv4-subnet-base')[0])
        prefixlen = int(self.prop('ipv4-prefixlen')[0])
        target = inet_pton(AF_INET, ipaddr)
        while prefixlen >= 8:
            if base[0] != target[0]: return False
            base = base[1:]
            target = target[1:]
            prefixlen -= 8
        if len(base) == 0: return True
        return ord(base[0]) == ord(target[0]) & -(1<<(8-prefixlen))
    def find_ipv4_subnet(self):
        """
        Find an <ipv4-subnet> resource that contains the address
        represented by this resource's <ipv4-addr> property.
        """
        targets = self.prop('ipv4-addr')
        subnets = Resource.objects.get(id='ipv4-subnet').rprop('type')
        return [ s for s in subnets
                 if [ True for t in targets if s.ipv4_subnet_contains(t) ] ]

# Useful mechanisms for generating Q objects relating to Resources
# Examples:
# qs = Resources.objects.filter(PQ(type=Resource('rack')))
# qs = Resources.objects.get(PQ(serial_number='12345')) # brave
def PQ(**kwargs):
    q = None
    for key, value in kwargs.items():
        key, sep, matchtype = key.partition('__')
        suffix = sep + matchtype
        key = key.replace('_', '-')
        if isinstance(value, Resource):
            assert(suffix == '')
            qq = models.Q(properties_fwd__predicate=key,
                          properties_fwd__object_res=value)
        else:
            qq = models.Q(properties_fwd__predicate=key,
                          **{"properties_fwd__object_lit" + suffix: value})
        if q == None:
            q = qq
        else:
            q &= qq
    if q == None:
        raise Exception("No kwargs supplied to PQ()")
    return q
def RPQ(**kwargs):
    q = None
    for key, value in kwargs.items():
        key, sep, matchtype = key.partition('__')
        suffix = sep + matchtype
        key = key.replace('_', '-')
        if isinstance(value, string_types):
            value = Resource(value)
        qq = models.Q(properties_rev__predicate=key,
                      **{"properties_rev__subject" + suffix: value})
        if q == None:
            q = qq
        else:
            q &= qq
    if q == None:
        raise Exception("No kwargs supplied to RPQ()")
    return q

# Mechanism for sorting by a property.  Works best for single-valued
# properties.  Usage:
#
# qs = qs.extra(**order_by_prop('label'))
# qs = qs.extra(**order_by_intprop('u-position'))

def _order_by_propexpr(varname, prop, propexpr):
    # This code is ugly and depends on undocumented details of the
    # Django ORM.
    return {
        "select": { varname:
              """SELECT %s FROM ucs_besplusplus_property obp
                  WHERE obp.subject_id = ucs_besplusplus_resource.id AND
                        obp.predicate_id = %%s LIMIT 1""" % (propexpr,) },
        "select_params": [ prop ],
        "order_by": [ varname ],
    }

def order_by_prop(prop):
    return _order_by_propexpr("obp_" + prop.replace('-', '_'), prop,
                              'object_lit')

def order_by_intprop(prop):
    return _order_by_propexpr("obip_" + prop.replace('-', '_'), prop,
                              'CAST (object_lit AS NUMERIC)')

# Useful "not equal" lookup
# Based on https://docs.djangoproject.com/en/1.11/howto/custom-lookups/
@models.Field.register_lookup
@models.ForeignObject.register_lookup
class NotEqual(models.Lookup):
    lookup_name = 'ne'
    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params

property_changed = Signal(providing_args=["oldprop", "newprop"])

@python_2_unicode_compatible
class Property(models.Model):
    class Meta:
        verbose_name_plural = "properties"
        # We'd like to have this, but it doesn't work on MySQL
        # "Specified key was too long; max key length is 767 bytes"
        #unique_together = (('subject', 'predicate',
        #                    'object_res', 'object_lit', 'source'))
    subject = models.ForeignKey(Resource,
                                related_name='properties_fwd',
                                on_delete=models.CASCADE)
    predicate = models.ForeignKey(Resource,
                                    related_name='properties_sideways',
                                    on_delete=models.CASCADE)
    object_res = models.ForeignKey(Resource, blank=True, null=True,
                                   related_name='properties_rev',
                                   on_delete=models.CASCADE)
    object_lit = models.CharField(max_length=1024, blank=True, null=True)
    source = models.ForeignKey(Resource, related_name='properties_from',
                               default=settings.BESPLUSPLUS_SELF_ID,
                               on_delete=models.PROTECT)
    @property
    def object(self):
        """
        The 'object' psuedo-property intelligently uses object_res or
        object_lit as appropriate.
        """
        if self.object_res:
            return self.object_res
        return self.object_lit
    @object.setter
    def object(self, value):
        if isinstance(value, Resource):
            self.object_res = value
            self.object_lit = None
        else:
            self.object_res = None
            self.object_lit = value
    @atomic
    def save(self, *args, **kwargs):
        "Override saving to avoid writing duplicate properties"
        # One might hope that the "unique_together" class option above
        # would enforce this, but it fails to do so on SQLite at least.
        # Anyway, we'd prefer duplicate insertions to vacuously succeed
        # rather than throwing an exception.
        if Property.objects.filter(subject_id = self.subject_id,
                                        predicate_id = self.predicate_id,
                                        object_res_id = self.object_res_id,
                                        object_lit = self.object_lit,
                                        source_id = self.source_id).exists():
            return
        # Note any existing property we might be replacing to put in the
        # history.
        old=None
        if self.id:
            try:
                old = Property.objects.get(id=self.id)
            except Property.DoesNotExist:
                pass
        super(Property, self).save(*args, **kwargs)
        property_changed.send(Property, oldprop=old, newprop=self)
    def clean(self):
        # Precisely one of object_res and object_lit must be set.
        if self.object_res:
            self.object_lit = None
        else:
            if self.object_lit == None:
                raise ValidationError("One of object_lit and object_res must "+
                                      "be set")
        super(Property, self).clean()
    def __str__(self):
        if self.object_res:
            return "%s %s %s ." % (self.subject, self.predicate,
                                   self.object_res)
        else:
            return "%s %s \"%s\" ." % (self.subject, self.predicate,
                                       self.object_lit)

# We use a post_delete signal rather than overriding Property.delete
# because the signal gets called when using QuerySet.delete, which
# Property.delete doesn't.
@receiver(models.signals.post_delete, sender=Property)
def property_deleted(instance, **kwargs):
    property_changed.send(Property, oldprop=instance, newprop=None)

class HistoryRecord(models.Model):
    class Meta:
        ordering = ["-when"]
    when = models.DateTimeField(auto_now_add=True, db_index=True)
    who = models.CharField(max_length=255, blank=True)
    how = models.TextField(blank=True)
    deleted = models.BooleanField(default=None)
    # Relations with resources should allow for the possibility that the
    # target resource has been deleted or merged (which looks like deletion
    # at the database level).
    relation_settings = {
        'related_name': '+', # Don't create a reverse relation
        'db_constraint': False,
        'on_delete': models.DO_NOTHING
        }
    property = models.ForeignKey(Property, **relation_settings)
    subject = models.ForeignKey(Resource, **relation_settings)
    predicate = models.ForeignKey(Resource, **relation_settings)
    object_res = models.ForeignKey(Resource, blank=True, null=True,
                                   **relation_settings)
    object_lit = models.CharField(max_length=1024, blank=True, null=True)
    source = models.ForeignKey(Resource, **relation_settings)

@receiver(property_changed, sender=Property)
def addtohistory(oldprop, newprop, **kwargs):
    """
    Generate a history entry for the provided property.  If oldprop
    is None, the property is being deleted.  If newprop is None, it's
    being created.  If neither, it's being updated.
    """
    def record_hr(p, deleted):
        hr = HistoryRecord()
        hr.deleted = deleted
        hr.property = p
        hr.subject_id = p.subject_id
        hr.predicate_id = p.predicate_id
        hr.object_res_id = p.object_res_id
        hr.object_lit = p.object_lit
        hr.source_id = p.source_id
        hr.save()
    if oldprop != None:
        record_hr(oldprop, True)
    if newprop != None:
        record_hr(newprop, False)
