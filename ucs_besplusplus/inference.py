from __future__ import unicode_literals

from django.utils.six import text_type

from .models import Resource, id_from_fqdn, Property, property_changed

me = 'inference'

def onfwdpropchange(p):
    def _decorator(action):
        def rcvpropchange(oldprop, newprop, **kwargs):
            if oldprop != None and oldprop.predicate_id == p:
                action(oldprop.subject)
            if newprop != None and newprop.predicate_id == p:
                action(newprop.subject)
        property_changed.connect(rcvpropchange, weak=False)
        return action
    return _decorator

def onrevpropchange(p):
    def _decorator(action):
        def rcvpropchange(oldprop, newprop, **kwargs):
            if (oldprop != None and oldprop.predicate_id == p
                    and oldprop.object_res != None):
                action(oldprop.object_res)
            if (newprop != None and newprop.predicate_id == p
                    and newprop.object_res != None):
                action(newprop.object_res)
        property_changed.connect(rcvpropchange, weak=False)
        return action
    return _decorator

@onfwdpropchange('fqdn')
def infer_alias(r):
    r.setprop('alias',
              [id_from_fqdn(fqdn) for fqdn in r.prop('fqdn')
                   if isinstance(fqdn, text_type)],
              source=me)

@onfwdpropchange('type')
def infer_type(r):
    # This implements the inference:
    # r <type> t and t <subclass-of> u => r <type> u
    # superclasses = Resource.objects.raw(
    #     '''SELECT p2.object_res_id AS id
    #        FROM ucs_besplusplus_property p1, ucs_besplusplus_property p2
    #        WHERE p1.subject_id = %s AND p1.predicate_id = 'type'
    #          AND p1.source_id != %s
    #          AND p1.object_res_id = p2.subject_id
    #          AND p2.predicate_id = 'subclass-of'
    #          AND p2.object_res_id IS NOT NULL
    #     ''', [r.id, me])
    superclasses = Resource.objects.filter(
        properties_rev__predicate_id='subclass-of',
        properties_rev__subject__properties_rev__predicate_id='type',
        properties_rev__subject__properties_rev__subject=r,
        properties_rev__subject__properties_rev__source_id__ne=me)
    r.setprop('type', superclasses, source=me)
@onrevpropchange('subclass-of')
def infer_rtype(r):
    members = Resource.objects.filter(
        properties_fwd__predicate_id='type',
        properties_fwd__source_id__ne=me,
        properties_fwd__object_res__properties_fwd__predicate_id='subclass-of',
        properties_fwd__object_res__properties_fwd__object_res=r)
    r.setrprop('type', members, source=me)

@onfwdpropchange('subclass-of')
def infer_subclass_of(r):
    supersuperclasses = Resource.objects.filter(
        properties_rev__predicate_id='subclass-of',
        properties_rev__subject__properties_rev__predicate='subclass-of',
        properties_rev__subject__properties_rev__subject=r)
    r.setprop('subclass-of', supersuperclasses, source=me)
@onrevpropchange('subclass-of')
def infer_rsubclass_of(r):
    subsubclasses = Resource.objects.filter(
        properties_fwd__predicate_id='subclass-of',
        properties_fwd__object_res__properties_fwd__predicate_id='subclass-of',
        properties_fwd__object_res__properties_fwd__object_res=r)
    r.setrprop('subclass-of', subsubclasses, source=me)

def infer_everything_everywhere():
    for r in Resource.objects.all():
        infer_subclass_of(r)
        infer_type(r)
