from __future__ import unicode_literals

# The purpose of this module is to register all Django signal handlers when it
# is imported, as suggested here:
#
# https://docs.djangoproject.com/en/2.0/topics/signals/

# Since signal handlers are generally registered by importing their modules,
# that's all we do.

from . import models
from . import inference
