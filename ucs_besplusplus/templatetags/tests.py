from __future__ import unicode_literals

from django.test import TestCase
from ..models import Resource
from .ucs_besplusplus import rack_power_url, pdu_power_url, box_power_url

class RackPowerURLTests(TestCase):
    def test_simple(self):
        Resource('label').save()
        r = Resource()
        r.save()
        r.addprop('label', 'RNB-26')
        self.assertEqual(rack_power_url(r),
            'https://rnb.infra.csi.cam.ac.uk/power/pdudetail.html?rack=rnb-26')
    def test_silly(self):
        Resource('label').save()
        r = Resource()
        r.save()
        r.addprop('label', 'RNB-<<')
        self.assertNotEqual(rack_power_url(r),
            'https://rnb.infra.csi.cam.ac.uk/power/pdudetail.html?rack=rnb-<<')

class PDUPowerURLTests(TestCase):
    def test_simple(self):
        Resource('label').save()
        r = Resource()
        r.save()
        r.addprop('label', 'pdu-rnb-a-k')
        self.assertEqual(pdu_power_url(r),
            'https://rnb.infra.csi.cam.ac.uk/power/detail.html?outlet=pdu-rnb-a-k')
    def test_silly(self):
        Resource('label').save()
        r = Resource()
        r.save()
        r.addprop('label', '12345')
        self.assertEqual(pdu_power_url(r), None)
    def test_unlabelled(self):
        Resource('label').save()
        r = Resource()
        self.assertEqual(pdu_power_url(r), None)
        
class BoxPowerURLTests(TestCase):
    def test_unusual(self):
        """We had problems with a WCDC PDU.  This tests such things."""
        Resource('label').save()
        Resource('incorporates').save()
        Resource('powers').save()
        p = Resource()
        p.save()
        p.addprop('label', "V14K2600130")
        o = Resource()
        o.save()
        p.addprop('incorporates', o)
        o.addprop('label', "A1")
        b = Resource()
        b.save()
        b.addprop('label', "gytrash")
        o.addprop('powers', b)
        self.assertEqual(box_power_url(b), None)
