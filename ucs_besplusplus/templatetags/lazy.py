from __future__ import unicode_literals

from django import template
import re

register = template.Library()

# The "lazy" tag renders its content if and only if the contents of the embedded
# block bounded by "content"/"endcontent" tags is non-empty.

@register.tag
def lazy(parser, token):
    before = parser.parse(('content',))
    parser.delete_first_token()
    inside = parser.parse(('endcontent',))
    parser.delete_first_token()
    after = parser.parse(('endlazy',))
    parser.delete_first_token()
    return LazyNode(before, inside, after)

class LazyNode(template.Node):
    def __init__(self, before, inside, after):
        self.before = before
        self.inside = inside
        self.after = after
    def render(self, context):
        inside = self.inside.render(context)
        if re.search(r'\S', inside):
            return (self.before.render(context) + inside +
                    self.after.render(context))
        return ''
