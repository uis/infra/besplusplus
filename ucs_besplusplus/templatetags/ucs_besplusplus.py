from __future__ import unicode_literals

from django import template
from django.utils.html import escape
from ..directory import directory_url, directory_name
from ..models import Resource
from math import gcd
import logging

register = template.Library()
logger = logging.getLogger(__name__)

@register.filter
def lookup_url(value):
    return directory_url(value)

@register.filter
def lookup_name(value):
    return directory_name(value)

@register.simple_tag
def rack_power_url(rack):
    """
    Return the URL for the power control/monitoring page for a rack.
    """
    labels = rack.prop('label')
    if not labels: return None
    label = labels[0]
    room = label.partition('-')[0].lower()
    rackid = label.lower()
    return escape(
        "https://%s.infra.csi.cam.ac.uk/power/pdudetail.html?rack=%s" %
        (room, rackid))

@register.simple_tag
def pdu_power_url(pdu):
    """
    Return the URL for the power control/monitoring page for a PDU.
    """
    try:
        labels = pdu.prop('label')
        label = labels[0]
        room = label.split('-')[1].lower()
        return escape(
            "https://%s.infra.csi.cam.ac.uk/power/detail.html?outlet=%s" %
            (room, label))
    except IndexError as e:
        return None

@register.simple_tag
def box_power_url(box):
    """
    Return the URL for the power control/monitoring page for a box.
    """
    labels = box.prop('label')
    if not labels: return None
    label = labels[0]
    # Need to work out which room the box is likely to be in.
    pdulabel = None
    for o in box.rprop('powers'):
        for p in o.rprop('incorporates'):
            for l in p.prop('label'):
                pdulabel = l
                break
    if pdulabel == None: return None
    try:
        room = pdulabel.split('-')[1].lower()
        return escape(
            "https://%s.infra.csi.cam.ac.uk/power/detail.html?outlet=%s" %
            (room, label))
    except IndexError as e:
        return None

@register.inclusion_tag('ucs_besplusplus/rack_snippet.html')
def rack_snippet(rack):
    contents = []
    strays = set()
    for r in rack.prop('contains'):
        # You can't put a literal in a rack.
        if not isinstance(r, Resource): continue
        bottom_us = r.prop('u-position')
        if not bottom_us:
            # In the rack somewhere, but we don't know where
            strays.add(r)
            continue
        bottom_u = int(bottom_us[0]) # Not meaningful to have more than one
        u_sizes = r.prop('u-size')
        if not u_sizes:
            u_sizes = ['1'] # good enough for display
        u_size = int(u_sizes[0]) # Not meaningful to have more than one

        top_u = bottom_u + u_size - 1
        while len(contents) < top_u:
            contents.append({ 'u_position': len(contents) + 1,
                              'columns': 0 })

        if 'boxes' not in contents[top_u - 1]:
            contents[top_u - 1]['boxes'] = []
        contents[top_u - 1]['boxes'].append(r)
        for i in range(u_size):
            contents[top_u - i - 1]['columns'] += 1

    for i, u in enumerate(contents):
        if u['columns'] == 0:
            logger.debug("no columns in U %d", i)
            if 'gap' in contents[i-1]:
                u['gap'] = contents[i-1]['gap'] + 1
                logger.debug("gap is now %d", u['gap'])
                del contents[i-1]['gap']
            else:
                u['gap'] = 1
            u['columns'] = 1
    totalcols = 1
    for cols in [u['columns'] for u in contents]:
        if cols > 0:
            totalcols *= cols / gcd(totalcols, cols)
    for u in contents:
        if u['columns'] > 0:
            u['colspan'] = totalcols / u['columns']
        else:
            u['colspan'] = totalcols
    logger.debug("%s", contents)
    return { 'rack_contents': contents, 'rack_strays': strays,
             'rack_cols': totalcols }
