from __future__ import print_function, unicode_literals

from django.conf import settings
import logging
logger = logging.getLogger(__name__)

from . import directory

class BesPlusPlusAuthMixin(object):
    def configure_user(self, user):
        user = super(BesPlusPlusAuthMixin, self).configure_user(user)
        # Set up default permissions.  This is probably too generous.
        logger.info("Configuring new user %s", user)
        if (not hasattr(settings, 'BESPLUSPLUS_ADMIN_GROUP') or
            settings.BESPLUSPLUS_ADMIN_GROUP in
            directory.groups_for_user(user.username)):
            logger.info("Granting admin permission to %s", user)
            user.is_staff = True
            user.is_active = True
            user.is_superuser = True
            user.save()
        return user

try:
    from ucamwebauth.backends import RavenAuthBackend
    class BesPlusPlusRavenBackend(BesPlusPlusAuthMixin, RavenAuthBackend):
        pass
except ImportError as e:
    pass

from django.contrib.auth.backends import RemoteUserBackend

class BesPlusPlusRemoteUserBackend(BesPlusPlusAuthMixin, RemoteUserBackend):
    pass

