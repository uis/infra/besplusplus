from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Resource, Property, HistoryRecord, PQ

class PropertyInline(admin.TabularInline):
    model = Property
    extra = 0
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "predicate":
            kwargs["queryset"]  = (
                Resource.objects.filter(PQ(type=Resource('property')))
                                .distinct())
        return (super(PropertyInline, self)
                    .formfield_for_foreignkey(db_field, request, **kwargs))

# When displaying a resource, make its properties editable in-line.
class SubjectInline(PropertyInline):
    fk_name = 'subject'
    raw_id_fields = ('object_res','source')

# Ditto reverse properties.
class ObjectInline(PropertyInline):
    model = Property
    fk_name = 'object_res'
    exclude = ('object_lit',)
    raw_id_fields = ('subject','source')
    verbose_name = 'Reverse Property'
    verbose_name_plural = 'Reverse Properties'

class ResourceAdmin(admin.ModelAdmin):
    inlines = [SubjectInline, ObjectInline]
    actions = ['merge_resources']
    def merge_resources(self, request, queryset):
        queryset[0].merge(queryset[1:])
    search_fields = ['properties_fwd__object_lit']

class HistoryAdmin(admin.ModelAdmin):
    raw_id_fields = ('property', 'subject', 'predicate', 'object_res',
                     'source')
    
# The standard admin site insists on presenting its own login box to
# unauthenticated users.  This can be worked around by subclassing
# AdminSite. This stops the autodiscovery magic from working because
# that relies on everything using django.contrib.admin.site, which is
# a vanilla AdminSite.  We take advantage of that and set up our own.

class RavenCompatibleAdminSite(admin.AdminSite):
    @method_decorator(login_required)
    def login(self, *args, **kwargs):
        return super(RavenCompatibleAdminSite, self).login(*args, **kwargs)

# This is the Bes++ internal admin site.
site = RavenCompatibleAdminSite()

# Register with both our site and the standard admin site, in case
# someone's using that.
for s in (site, admin.site):
    s.register(Resource, ResourceAdmin)
    s.register(HistoryRecord, HistoryAdmin)
