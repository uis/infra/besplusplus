from __future__ import print_function, unicode_literals

from django.db.transaction import atomic
import yaml

from ..models import Resource, PQ

accept = "text/x.yaml,application/json"

@atomic
def do_import(infile, source=None):
    setup()
    dkx2s = yaml.safe_load(infile)
    for k, v in dkx2s.items():
        import_dkx2(k, v)

def ensure_resource(id, **props):
    r = Resource(id)
    r.save()
    for k, v in props.items():
        obj = Resource(v)
        obj.save()
        r.addprop(k, obj)

def setup():
    for p in ['label', 'serial-number', 'fqdn', 'firmware-version',
              'dkx2-cim', 'console']:
        ensure_resource(p, type='property')
    for c in ['dkx2-cim', 'dkx2', 'box']:
        ensure_resource(c, type='class')
    
def import_dkx2(name, ports):
    rs = Resource.objects.filter(PQ(fqdn=name))
    if rs:
        r = rs[0]
    else:
        r = Resource()
        r.save()
    if not r.fwd('label'):
        r.addprop('label', name)
    r.setprop('fqdn', [name])
    r.addprop('type', Resource("dkx2"))
    for port in ports:
        import_port(r, port)

def import_port(dkx2, port):
    # "Admin" and "FG" ports don't seem to correspond to downstream ports.
    # "Not Available" indicates that the CIM is unpowered or disconnected.
    if port["PortType"] in ('Admin', 'FG', 'Not Available'): return
    rs = Resource.objects.filter(PQ(serial_number=port["SerialNo"]))
    if rs:
        r = rs[0]
    else:
        r = Resource()
        r.save()
    set_cim_properties(dkx2, port, r)
        
def set_cim_properties(dkx2, dkx2data, resource):
    resource.addprop('type', Resource("dkx2-cim"))
    resource.setprop('label', [dkx2data['Name']])
    resource.addprop('serial-number', dkx2data['SerialNo'])
    if dkx2data['FwVersion'] != 'N/A':
        resource.setprop('firmware-version', [dkx2data['FwVersion']])
    resource.setrprop('dkx2-cim', [dkx2])
    infer_host(resource)
            
def infer_host(cim):
    labels = cim.fwd('label')
    if not cim.rev('console') and labels:
        q = PQ(label=labels[0])
        for label in labels[1:]:
            q |= PQ(label=label)
        boxes = Resource.objects.filter(q).filter(PQ(type=Resource('box')))
        if len(boxes) == 1 and not boxes[0].fwd('console'):
            # Only one candidate and it doesn't yet have a console.
            # XXX should allow ones with a service processor console.
            cim.addrprop('console', boxes[0])

