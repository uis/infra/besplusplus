from __future__ import unicode_literals

from django.test import TestCase
from ..models import Resource, Property
from os import path

class DKX2ImportTests(TestCase):
    def test_dkx2_import(self):
        # Set up a box for it to find
        Resource("box").save()
        label = Resource("label")
        label.save()
        label.addprop("type", Resource("property"))
        
        b = Resource()
        b.save()
        b.addprop("label", "tufted")
        b.addprop("type", Resource("box"))
        import ucs_besplusplus.importer.dkx2 as dkx2
        dkx2.do_import(
            open(path.join(path.dirname(__file__), "test_dkx2.yaml")))
        p = Property.objects.filter(predicate_id='type',
                                    object_res_id='dkx2')
        self.assertEqual(p.count(), 7)
        p = Property.objects.filter(predicate_id='type',
                                    object_res_id='dkx2-cim')
        self.assertEqual(p.count(), 102)
        p = Property.objects.filter(subject_id='label', predicate_id='type',
                                    object_res_id='property')
        self.assertEqual(p.count(), 1)
        self.assertEqual(len(b.fwd("console")), 1)
        # Importers should be idempotent, so try it again.
        dkx2.do_import(
            open(path.join(path.dirname(__file__), "test_dkx2.yaml")))
        p = Property.objects.filter(predicate_id='type',
                                    object_res_id='dkx2')
        self.assertEqual(p.count(), 7)
        p = Property.objects.filter(predicate_id='type',
                                    object_res_id='dkx2-cim')
        self.assertEqual(p.count(), 102)
        p = Property.objects.filter(subject_id='label', predicate_id='type',
                                    object_res_id='property')
        self.assertEqual(p.count(), 1)
        self.assertEqual(len(b.fwd("console")), 1)
