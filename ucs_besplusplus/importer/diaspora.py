from __future__ import print_function, unicode_literals

import yaml

from ..models import Resource, Property
from django.db.transaction import atomic

def mkres(type, label):
    # First, see if we can find this rack already
    p = Property.objects.filter(predicate_id='label', object_lit=label)
    if p:
        r = p[0].subject
    else:
        r = Resource()
        l = Property(subject=r, predicate_id='label', object_lit=label)
        r.save()
        l.save()
    if type not in r.fwd('type'):
        t = Property(subject=r, predicate_id='type', object_res_id=type)
        t.save()
    return r

@atomic
def do_import(boxesfile):
    boxes = yaml.safe_load(boxesfile)
    for (name, info) in boxes.items():
        b = mkres('box', name)
        if 'to' in info:
            r = mkres('rack', info['to'].upper())
            r.addprop('contains', b)
        if 'inv' in info:
            b.addprop('ucs-inv', info['inv'])
