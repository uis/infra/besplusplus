from __future__ import print_function, unicode_literals

from io import TextIOBase, TextIOWrapper
import csv

from .simplejson import do_import as do_simplejson_import
from ..models import id_from_fqdn

# Jackdaw claims its output is "application/octet-stream; charset=utf-8",
# but I can't bring myself to ask for that.
accept = "*/*"

class JackdawTSV(csv.Dialect):
    delimiter = str("\t")
    quoting = csv.QUOTE_NONE
    strict = True
    lineterminator = "\r\n"

def to_simplejson(infile):
    if not isinstance(infile, TextIOBase):
        infile = TextIOWrapper(infile, encoding='utf-8')
    r = csv.DictReader(infile, dialect=JackdawTSV)
    seen = set()
    mzones = set()
    resources = []
    for row in r:
        resources.append(import_row(row, seen))
    return resources

def do_import(infile, source=None):
    do_simplejson_import(to_simplejson(infile), source)

def import_row(row, seen):
    r = { }
    r['@id'] = id_from_fqdn(row['name'])
    r['fqdn'] = row['name']
    if 'address' in row:
        # list_ops dump; single IPv4 address
        r['ipv4-address'] = row['address']
        r['@type'] = 'ipreg-box'
    elif 'addresses' in row:
        # xlist_ops dump; multiple addresses
        r['ipv4-address'] = []
        r['ipv6-address'] = []
        if row['addresses'] != '':
            for addr in row['addresses'].split(','):
                if ':' in addr:
                    r['ipv6-address'].append(addr)
                elif '.' in addr:
                    r['ipv4-address'].append(addr)
                else:
                    raise Exception("Invalid address in input: %s" % addr)
                # intuit the type of record
                if 'location' in row:
                    r['@type'] = 'ipreg-box'
                elif 'boxes' in row:
                    r['@type'] = 'ipreg-vbox'
                else:
                    raise Exception("Cannot intuit record type")
    else:
        raise Exception("Cannot intuit record type")
    return r
