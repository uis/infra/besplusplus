from __future__ import unicode_literals

from django.test import TestCase
from ..models import Resource

from django.utils.six import StringIO

import ucs_besplusplus.importer.simplejson as sj
import ucs_besplusplus.importer.ipreg as ipreg

def setup():
    sj.do_import([{'@id': 'foo', 'fqdn': 'foo.1.example'},
                  {'@id': 'importer'}])

class IPRegImportTests(TestCase):
    def test_simple(self):
        setup()
        ipreg.do_import(StringIO(
"""name\tmzone\taddress
foo.1.example\tEG\t198.51.100.73
"""), source='importer')
        self.assertEqual(Resource.objects.get(id='foo').prop('ipv4-address'),
                         ["198.51.100.73"])
        ipreg.do_import(StringIO(
"""name\tmzone\taddress
bar.1.example\tEG\t198.51.100.73
"""), source='importer')
        self.assertEqual(Resource.objects.get(id='foo').prop('ipv4-address'),
                         [])
