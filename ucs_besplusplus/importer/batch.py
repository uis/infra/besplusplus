from __future__ import print_function, unicode_literals

from ..models import Resource, Property

class BatchImportJob(set):
    def __init__(self, source_id):
        self.source_id = source_id
    def intern(self, id):
        return ResourceProxy(self, id)
    def apply(self):
        termcache = { None: None }
        def canonicalise_term(id):
            if id in termcache: return termcache[id]
            termcache[id] = Resource.objects.intern(id).id
            return termcache[id]
        def canonicalise_statement(stmt):
            return (canonicalise_term(stmt[0]),
                    canonicalise_term(stmt[1]),
                    canonicalise_term(stmt[2]), stmt[3])
        source = Resource.objects.intern(self.source_id)
        canonical = set([canonicalise_statement(s) for s in self])
        for p in Property.objects.filter(source=source):
            ps = (p.subject_id, p.predicate_id, p.object_res_id, p.object_lit)
            if ps in canonical:
                canonical.remove(ps)
            else:
                p.delete()
        for ps in canonical:
            if ps[2] != None:
                Property.objects.create(
                    subject_id=ps[0],
                    predicate_id=ps[1],
                    object_res_id=ps[2],
                    source=source)
            else:
                Property.objects.create(
                    subject_id=ps[0],
                    predicate_id=ps[1],
                    object_lit=ps[3],
                    source=source)

# A fake Resource that collects statements during a batch import.
class ResourceProxy(object):
    def __init__(self, job, id):
        self.job = job
        self.id = id
    def addprop(self, k, obj):
        if not isinstance(k, ResourceProxy):
            k = self.job.intern(k)
        if isinstance(obj, ResourceProxy):
            self.job.add((self.id, k.id, obj.id, None))
        else:
            self.job.add((self.id, k.id, None, obj))
    def setprop(self, k, objs):
        for obj in objs:
            self.addprop(k, obj)
    def addrprop(self, k, subj):
        if not isinstance(subj, ResourceProxy):
            subj = self.job.intern(subj)
        if not isinstance(k, ResourceProxy):
            k = self.job.intern(k)
        self.job.add((subj.id, k.id, self.id, None))
    def setrprop(self, k, subjs):
        for subj in subjs:
            self.addrprop(k, subj)
