from __future__ import print_function, unicode_literals

from django.db.transaction import atomic
import json

from ..models import Resource, PQ

def ensure_resource(id, **props):
    r = Resource(id)
    r.save()
    for k, v in props.items():
        obj = Resource(v)
        obj.save()
        r.addprop(k, obj)

# ipv4-address, fqdn, mac-address, dhcp-server-for, vmware-instance-uuid, vcenter-uuid
        
def setup():
    for p in ['vmware-instance-uuid', 'console',
              'ipv4-address', 'mac-address', 'fqdn', 'dhcp-server-for']:
        ensure_resource(p, type='property')
    for c in ['vmware-vm']:
        ensure_resource(c, type='class')

@atomic
def do_import(infile):
    setup()
    op = json.load(infile)
    if op['opcode'] == 'delete':
        vms = Resource.objects.filter(
            PQ(vmware_instance_uuid=op['instance_uuid']))
        for vm in vms:
            for p in ['ipv4-address', 'mac-address', 'fqdn']:
                vm.setprop(p, [])
            vm.setrprop('dhcp-server-for', [])
        # Leave actual deletion for when we get an update from VMware.
    elif op['opcode'] == 'update':
        vcs = Resource.objects.filter(
            PQ(vmware_instance_uuid=op['vcenter_instance_uuid']))
        vms = Resource.objects.filter(
            PQ(vmware_instance_uuid=op['instance_uuid']))
        if not vms:
            vms = [Resource()]
            vms[0].save()
        for vm in vms:
            vm.addprop('type', Resource('vmware-vm'))
            vm.addprop('vmware-instance-uuid', op['instance_uuid'])
            for p in ['ipv4-address', 'mac-address', 'fqdn']:
                if p in op:
                    vm.setprop(p, [op[p]])
                else:
                    vm.setprop(p, [])
            for vc in vcs:
                vm.addprop('console', vc)
            if 'dhcp-server' in op:
                vm.setrprop('dhcp-server-for', [Resource(op['dhcp-server'])])
            else:
                vm.setrprop('dhcp-server-for', [])
