from __future__ import unicode_literals

from django.test import TestCase
from .batch import BatchImportJob
from ..models import Resource, HistoryRecord

import ucs_besplusplus.importer.simplejson as sj

class BatchImportTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        sj.do_import([
            {'@id': 'alias', '@type': 'property'},
            ])
    def test_construct(self):
        j = BatchImportJob("test-source")
        self.assertEqual(len(j), 0)
    def make_sample(self):
        j = BatchImportJob("test-source")
        r = j.intern("foo")
        r.addprop("bar", "ba\2013z")
        r.addprop("bar", j.intern("quux"))
        r.addprop("bar", j.intern("quux"))
        r.addrprop("spoo", j.intern("splink"))
        return j
    def test_simple(self):
        j = self.make_sample()
        self.assertEqual(set(j), {("foo", "bar", None, "ba\2013z"),
                                  ("foo", "bar", "quux", None),
                                  ("splink", "spoo", "foo", None)})
    def test_apply(self):
        j = self.make_sample()
        j.apply()
        self.assertEqual(Resource.objects.get(id="splink").prop("spoo"),
                             [Resource("foo")])
    def test_dereference(self):
        thing = Resource.objects.intern("thing")
        thing.addprop("alias", "splink")
        j = self.make_sample()
        j.apply()
        self.assertEqual(Resource.objects.get(id="thing").prop("spoo"),
                             [Resource("foo")])
    def test_overwrite_nochange(self):
        thing = Resource.objects.intern("thing")
        thing.addprop("alias", "splink")
        j = self.make_sample()
        hrcount_before = HistoryRecord.objects.all().count()
        j.apply()
        hrcount_between = HistoryRecord.objects.all().count()
        j.apply()
        self.assertEqual(Resource.objects.get(id="thing").prop("spoo"),
                             [Resource("foo")])
        hrcount_after = HistoryRecord.objects.all().count()
        self.assertGreater(hrcount_between, hrcount_before)
        self.assertEqual(hrcount_after, hrcount_between)
    def test_overwrite_empty(self):
        thing = Resource.objects.intern("thing")
        thing.addprop("alias", "splink")
        j = self.make_sample()
        hrcount_before = HistoryRecord.objects.all().count()
        j.apply()
        hrcount_between = HistoryRecord.objects.all().count()
        j = BatchImportJob("test-source")
        j.apply()
        self.assertEqual(Resource.objects.get(id="thing").prop("spoo"), [])
        hrcount_after = HistoryRecord.objects.all().count()
        self.assertGreater(hrcount_after, hrcount_between)
