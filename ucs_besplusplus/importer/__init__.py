from __future__ import print_function, unicode_literals

import logging
import requests
from django.conf import settings
from django.utils.six import BytesIO
from django.utils.six.moves.http_cookiejar import MozillaCookieJar

from ..models import Property
from . import dkx2, ipreg, simplejson

import_props = {
    "dkx2-import-from": dkx2,
    "ipreg-import-from": ipreg,
    "simplejson-import-from": simplejson,
}

logger = logging.getLogger(__name__)
    
class AutoImportError(Exception):
    pass

def autoimport_prop(p):
    """
    Given a Property linking a resource to an import URL, import
    from it.
    """
    cj = MozillaCookieJar(settings.BESPLUSPLUS_IMPORT_COOKIEJAR)
    cj.load()
    module = import_props[p.predicate]
    url = p.object
    if not url.startswith("https:"):
        raise AutoImportError("Will not import from a non-HTTPS URL")
    r = requests.get(url, verify=True,
                     headers={"Accept": module.accept,
                              "User-agent": "Bes++"},
                     cookies=cj)
    r.raise_for_status()
    module.do_import(BytesIO(r.content), p.subject_id)

def can_autoimport(r):
    return Property.objects.filter(subject=r,
                                predicate__in=import_props.keys()).exists()

def autoimport_res(r):
    for p in Property.objects.filter(subject=r,
                                    predicate__in=import_props.keys()):
        autoimport_prop(p)

def autoimport_all():
    for p in Property.objects.filter(predicate__in=import_props.keys()):
        logger.info("importing from <%s>", p.object)
        try:
            autoimport_prop(p)
        except Exception as e:
            logger.error("import from <%s> failed: %s", p.object, e)
