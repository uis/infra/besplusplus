from __future__ import print_function, unicode_literals

from collections import Mapping
from io import IOBase, TextIOBase, TextIOWrapper

from django.db.transaction import atomic
from django.utils.six import string_types
import json

from .batch import BatchImportJob
from ..models import Resource

# Simple JSON importer, originally intended for use when testing.

# Strictly, we only accept the Bes++ dialect of JSON-LD.

accept = "application/json,application/ld+json;q=0.5"

class SimpleJSONImportJob(object):
    def __init__(self, source=None):
        if source != None:
            self.job = BatchImportJob(source)
            self.intern = self.job.intern
        else:
            self.job = None
            self.intern = Resource.objects.intern

    @atomic
    def do_import(self, infile):
        if isinstance(infile, IOBase):
            if not isinstance(infile, TextIOBase):
                infile = TextIOWrapper(infile, encoding='utf-8')
            indict = json.load(infile)
        elif isinstance(infile, string_types):
            indict = json.loads(infile)
        else:
            indict = infile
        if not isinstance(indict, Mapping):
            indict = {'@graph': indict}
        if '@graph' not in indict:
            indict = {'@graph': [indict]}
        for jr in indict['@graph']:
            self.import_resource(jr)
        
    def import_resource(self, jr):
        if '@id' in jr:
            r = self.intern(jr['@id'])
        else:
            assert(self.job == None)
            r = Resource.objects.create()
        if '@type' in jr:
            self.import_prop(r, 'type', jr['@type'], force_resource=True)
        if '@reverse' in jr:
            for rp in jr['@reverse']:
                self.import_rprop(r, rp, jr['@reverse'][rp])
        for p in jr:
            if p.startswith('@'): continue
            self.import_prop(r, p, jr[p])
        return r
        
    def import_prop(self, r, key, vals, force_resource=False):
        self.ensure_prop(key)
        if not isinstance (vals, list):
            vals = [vals]
        for val in vals:
            if force_resource and not isinstance(val, dict):
                val = { '@id': val }
            if isinstance(val, dict):
                obj = self.import_resource(val)
                r.addprop(key, obj)
            else:
                r.addprop(key, val)

    def import_rprop(self, r, key, vals):
        self.ensure_prop(key)
        if not isinstance (vals, list):
            vals = [vals]
        for val in vals:
            if not isinstance(val, dict):
                val = { '@id': val }
            subj = self.import_resource(val)
            r.addrprop(key, subj)

    def ensure_prop(self, pn):
        p = self.intern(pn)
        p.addprop('type', self.intern('property'))

    def apply(self):
        if self.job:
            self.job.apply()

@atomic
def do_import(infile, source=None):
    job = SimpleJSONImportJob(source)
    job.do_import(infile)
    job.apply()
