from __future__ import print_function, unicode_literals

from django.db.transaction import atomic
from django.core.exceptions import ObjectDoesNotExist
import csv

from ..models import Resource, PQ, RPQ

@atomic
def do_import(infile):
    # setup()
    r = csv.DictReader(infile)
    seen_boxes = set()
    seen_racks = set()
    for row in r:
        import_row(row, seen_boxes, seen_racks)
    for rack in seen_racks:
        for box in rack.prop('contains'):
            if box not in seen_boxes:
                print("Box %s (in %s) not found in CSV" %
                      (box, rack))
        
def find_rack(row):
    label = row['Rack Number']
    if "-" not in label: label = "RNB-" + label
    return (Resource.objects.filter(PQ(type=Resource('rack')))
            .get(PQ(label=label)))
        
def find_box(rack, row):
    # Try to find an existing box corresponding with this row
    rack_contents = Resource.objects.filter(RPQ(contains=rack))
    if row['I/N'] != "":
        rs = rack_contents.filter(PQ(ucs_inv=row['I/N']))
        if len(rs) == 1:
            return rs[0]
        elif len(rs) > 1:
            print("I/N %s (%s): found multiple matches" %
                  (row['I/N'], row['Machine Name']))
    if row['S/N Number'] != "":
        rs = rack_contents.filter(PQ(serial_number=row['S/N Number']))
        if len(rs) == 1:
            return rs[0]
        elif len(rs) > 1:
            print("S/N %s (%s): found multiple matches" %
                  (row['S/N Number'], row['Machine Name']))
    # Label?
    if row['Machine Name'] != "":
        rs = (rack_contents.filter(PQ(type=Resource('box')))
              .filter(PQ(label__iexact=row['Machine Name'].strip())))
        if len(rs) == 1:
            return rs[0]

typemap = {
    ('Apple', 'Mac Mini'): Resource('mac-mini'),
    ('Apple Mac', 'Xserve'): Resource('xserve'),
    ('Apple Mac', 'Xserver'): Resource('xserve'),
    ('Arista', '7050s-52'): Resource('arista-7050s-52'),
    ('Aruba', '3600'): Resource('aruba-3600'),
    ('Aruba', '7220'): Resource('aruba-7220'),
    ('Asus', 'RS300-H8-PS12'): Resource('rs300-h8-ps12'),
    ('Avantek', 'SR1695 WBAC'): Resource('sr1695wbac'),
    ('Cisco', '2900 series'): Resource('cisco-2901'),
    ('Cisco', '2901'): Resource('cisco-2901'),
    ('Cisco', '3800 series'): Resource('cisco-3845'),
    ('Cisco', 'ASA 5510'): Resource('asa-5510'),
    ('Cisco', '5520'): Resource('asa-5520'),
    ('Cisco', 'ASA 5540'): Resource('asa-5540'),
    ('Cisco', 'ASA 5540 series'): Resource('asa-5540'),
    ('Cisco', 'ATA 186'): Resource('ata-186'),
    ('Cisco', 'Catalyst 3750x'): Resource('catalyst-3750x'),
    ('Cisco', 'MCS 7800 series'): Resource('mcs-7800'),
    ('Cisco', 'UCS C210 M2'): Resource('ucs-c210-m2'),
    ('Data Direct Networks', '6620 Raid'): Resource('ddn-6620'),
    ('Dataon', '1600'): Resource('dataon-1600'),
    ('dataON', '1600'): Resource('dataon-1600'),
    ('Dell', 'PowerConnect 8132'): Resource('powerconnect-8132'),
    ('Dell', 'Powerconnect 8132'): Resource('powerconnect-8132'),
    ('Dell', 'PowerEdge 1950'): Resource('poweredge-1950'),
    ('Dell', 'PowerEdge 2950'): Resource('poweredge-2950'),
    ('Dell', 'Poweredge 2950'): Resource('poweredge-2950'),
    ('Dell', 'PowerEdge R410'): Resource('poweredge-r410'),
    ('Dell', 'PowerEdge R420'): Resource('poweredge-r420'),
    ('Dell', 'PowerEdge R515'): Resource('poweredge-r515'),
    ('Dell', 'PowerEdge R610'): Resource('poweredge-r610'),
    ('Dell', 'PowerEdge R620'): Resource('poweredge-r620'),
    ('Dell', 'PowerEdge R710'): Resource('poweredge-r710'),
    ('Dell', 'PowerEdge R715'): Resource('poweredge-r715'),
    ('Dell', 'PowerEdge R720'): Resource('poweredge-r720'),
    ('Dell', 'PowerEdge R720xd'): Resource('poweredge-r720xd'),
    ('Dell', 'PowerEdge SC1425'): Resource('poweredge-sc1425'),
    ('Dell', 'PowerEdge T710'): Resource('poweredge-t710'),
    ('Dell', 'PowerVault MD1200'): Resource('powervault-md1200'),
    ('Dell', 'PowerVault MD1220'): Resource('powervault-md1220'),
    ('Dell', 'PowerVault NX3100'): Resource('powervault-nx3100'),
    ('Dell', 'PowerVault MD3220'): Resource('powervault-md3220'),
    ('Dell', 'PowerVault MD3220i'): Resource('powervault-md3220i'),
    ('Dell', 'PowerVault MD3260'): Resource('powervault-md3220'),
    ('Dell', 'Powervault 3660i'): Resource('powervault-md3660i'),
    ('Dell', 'PowerVault MD3660i'): Resource('powervault-md3660i'),
    ('Extreme', 'EPS-C'): Resource('eps-c'),
    ('Extreme', 'Summit X450e-48p'): Resource('x450e-48p'),
    ('Extreme', 'Summit X460-24x'): Resource('x460-24x'),
    ('Extreme', 'Summit X460-24X'): Resource('x460-24x'),
    ('Extreme', 'Summit X460-48p'): Resource('x460-48p'),
    ('HP', 'Proliant DL360 G6'): Resource('proliant-dl360-g6'),
    ('HP', 'Proliant DL 380 G5'): Resource('proliant-dl380-g5'),
    ('HP', 'Proliant DL 380 G6'): Resource('proliant-dl380-g6'),
    ('HP', 'Proliant DL380 G6'): Resource('proliant-dl380-g6'),
    ('IBM', 'System x3650 M4'): Resource('x3650m4'),
    ('Promise', 'Pegasus R4'): Resource('pegasus-r4'),
    ('Qlogic', 'SANbox'): Resource('sanbox'),
    ('Raritan', 'PX'): Resource('pdu'),
    ('Raritan', 'PX20'): Resource('pdu'),
    ('Raritan', 'PXE-1488'): Resource('pdu'),
    ('Raritan', 'KX2-116'): Resource('dkx2'),
    ('Raritan', 'KX2-132'): Resource('dkx2'),
    ('Sun', 'Sun Fire X4100'): Resource('sun-fire-x4100'),
    ('Sun Microsystems', 'SunFire X4100'): Resource('sun-fire-x4100'),
    ('Sun Microsystems', 'Sun Fire X4100'): Resource('sun-fire-x4100'),
    ('Sun Microsystems', 'SunFire X4150'): Resource('sun-fire-x4150'),
    ('Sun', 'Sunfire X4150'): Resource('sun-fire-x4150'),
    ('Sun Microsystems', 'Sun Fire X4150'): Resource('sun-fire-x4150'),
    ('Sun Microsystems', 'SunFire X4200'): Resource('sun-fire-x4200'),
    ('Sun Microsystems', 'SunFire X4500'): Resource('sun-fire-x4500'),
    ('Sun Microsystems', 'Ultra 5'): Resource('ultra-5'),
    ('Supermicro', 'Fat-Twin F517H6-FT'): Resource('superserver-f517h6-ft'),
    }
        
def import_row(row, seen_boxes, seen_racks):
    if row['Rack Number'] == '': return
    rack = find_rack(row)
    seen_racks.add(rack)
    box = find_box(rack, row)
    if not box:
        box = Resource()
        box.save()
    box.addprop('type', Resource('box'))
    for a, b in [('S/N Number', 'serial-number'),
                 ('I/N', 'ucs-inv'),
                 ('Machine Name', 'label'),
                 ('Item Description', 'oper-description'),
                 ('Make', 'oper-make'),
                 ('Model', 'oper-model')]:
        if row[a] != '':
            box.addprop(b, row[a])
    pos = row['U Position in rack from bottom']
    if pos not in ('','On floor at rear of rack'):
        y0, dummy, y1 = pos.partition('-')
        if y1 == '': y1 = y0
        box.setprop('u-position', [y0])
        box.setprop('u-size', [str(int(y1)-int(y0) + 1)])
        box.setrprop('contains', [rack])
    make_model = (row['Make'].strip(), row['Model'].strip())
    if make_model != ('', ''):
        if make_model in typemap:
            box.addprop('type', typemap[make_model])
            box.setprop('oper-make', [])
            box.setprop('oper-model', [])
        else:
            print("Unknown make/model: %s" % (make_model,))
    for col, suffix in [("Black Power Outlet", "-k"),
                        ("Orange Power Outlet", "-o"),
                        ("Blue Power Outlet", "-u")]:
        if row[col] not in ('', 'Not Connected', 'Not plugged in', 'Unplugged'):
            try:
                pdu = (Resource.objects.filter(RPQ(contains=rack))
                   .filter(PQ(type=Resource('pdu')))
                   .distinct().get(PQ(label__endswith=suffix)))
                outlet = (Resource.objects.filter(RPQ(incorporates=pdu))
                      .get(PQ(label=row[col])))
                box.addrprop('powers', outlet)
            except ObjectDoesNotExist:
                print("Couldn't find power outlet: %s %s number %s" %
                      (rack, col, row[col]))
    seen_boxes.add(box)
