from __future__ import unicode_literals

from django.test import TestCase
from django.utils.six import StringIO

from ..models import Resource, PQ
from . import vmcontrol

class VMControlTests(TestCase):
    def test_create(self):
        Resource('vmware-instance-uuid').save()
        vc = Resource()
        vc.save()
        vc.setprop('vmware-instance-uuid',
                   ['703cceed-dfb0-41d4-9d6a-afe192b2a3ce'])
        dh = Resource()
        dh.save()
        dh = Resource(dh.id)
        vmcontrol.do_import(StringIO("""
        { "opcode": "update",
          "instance_uuid": "7e3d6467-4b8c-4815-99fc-779a21cee002",
          "ipv4-address": "198.51.100.42",
          "mac-address": "f1:01:02:03:04:05",
          "fqdn": "vmthing.example",
          "dhcp-server": "%s",
          "vcenter_instance_uuid": "703cceed-dfb0-41d4-9d6a-afe192b2a3ce" }
        """ % (dh.id,)))
        vm = Resource.objects.get(
            PQ(vmware_instance_uuid='7e3d6467-4b8c-4815-99fc-779a21cee002'))
        self.assertEqual(vm.prop('ipv4-address'), ['198.51.100.42'])
        self.assertEqual(vm.rprop('dhcp-server-for'), [dh])
        self.assertEqual(vm.prop('console'), [vc])
        vmcontrol.do_import(StringIO("""
        { "opcode": "update",
          "instance_uuid": "7e3d6467-4b8c-4815-99fc-779a21cee002",
          "ipv4-address": "198.51.100.43",
          "mac-address": "f1:01:02:03:04:05",
          "fqdn": "vmthing.example",
          "dhcp-server": "%s",
          "vcenter_instance_uuid": "703cceed-dfb0-41d4-9d6a-afe192b2a3ce" }
        """ % (dh.id,)))
        self.assertEqual(vm.prop('ipv4-address'), ['198.51.100.43'])
        vmcontrol.do_import(StringIO("""
        { "opcode": "delete",
          "instance_uuid": "7e3d6467-4b8c-4815-99fc-779a21cee002" }
        """ ))
        self.assertEqual(vm.prop('ipv4-address'), [])
