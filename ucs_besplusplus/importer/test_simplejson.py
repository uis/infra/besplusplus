from __future__ import unicode_literals

from django.test import TestCase
from ..models import Resource

class JsonImportTests(TestCase):
    def test_simple(self):
        import ucs_besplusplus.importer.simplejson as j
        j.do_import('{ "@id": "mook", "murfl": "spoo" }')
        self.assertEqual(Resource.objects.get(id="mook").prop("murfl"),
                         ["spoo"])
    def test_array(self):
        import ucs_besplusplus.importer.simplejson as j
        j.do_import('[{ "@id": "mook", "murfl": "spoo" }]')
        self.assertEqual(Resource.objects.get(id="mook").prop("murfl"),
                         ["spoo"])
