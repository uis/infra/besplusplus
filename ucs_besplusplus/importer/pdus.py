from __future__ import print_function, unicode_literals

from django.db.transaction import atomic
import yaml

from ..models import Resource, PQ, RPQ

@atomic
def do_import(infile):
    setup()
    pdus = yaml.safe_load(infile)
    for k, v in pdus.items():
        import_pdu(k, v)

def ensure_resource(id, **props):
    r = Resource(id)
    r.save()
    for k, v in props.items():
        obj = Resource(v)
        obj.save()
        r.addprop(k, obj)

def setup():
    for p in ['label', 'incorporates', 'sysadmin', 'fqdn']:
        ensure_resource(p, type='property')
    for c in ['pdu', 'pdu-outlet', 'box', 'psu']:
        ensure_resource(c, type='class')
    
def import_pdu(name, outlets):
    # label is all we've got in the input to identify PDUs
    rs = Resource.objects.filter(PQ(label=name))
    if rs:
        r = rs[0]
    else:
        r = Resource()
        r.save()
    r.setprop('label', [name])
    r.setprop('fqdn', ["%s.infra" % (name,)])
    r.addprop('type', Resource("pdu"))
    r.addprop('type', Resource("box"))
    # All PDUs are managed by Platforms
    r.addprop('sysadmin', '101128')
    for outletnum, outletname in outlets.items():
        os = (Resource.objects.filter(RPQ(incorporates=r))
                .filter(PQ(label=outletnum)))
        if os:
            o = os[0]
        else:
            o = Resource()
            o.save()
        o.setprop('label', [outletnum])
        o.setrprop('incorporates', [r])
        o.addprop('type', Resource('pdu-outlet'))
        if not o.fwd('powers') and outletname:
            boxname = str(outletname).partition(':')[0]
            boxes = (Resource.objects.filter(PQ(label=boxname))
                     .filter(PQ(type=Resource('box'))))
            if len(boxes) == 0:
                print("Creating box for %s" % outletname)
                box = Resource()
                box.save()
                box.addprop('type', Resource('box'))
                box.addprop('label', boxname)
                boxes = [box]
            if len(boxes) == 1:
                # Only one candidate.
                box = boxes[0]
                psuname = str(outletname).partition(':')[2]
                if psuname:
                    psus = (Resource.objects.filter(RPQ(incorporates=box))
                            .filter(PQ(type=Resource('psu')))
                            .filter(PQ(label=psuname)))
                    if len(psus) == 0:
                        print("Creating PSU for %s" % outletname)
                        psu = Resource()
                        psu.save()
                        psu.addprop('type', Resource('psu'))
                        psu.addprop('label', psuname)
                        psu.addrprop('incorporates', box)
                        o.addprop('powers', psu)
                    elif len(psus) == 1:
                        o.addprop('powers', psus[0])
                    else:
                        print("Could not find PSU for %s" % outletname)
                else:
                    o.addprop('powers', box)
            else:
                print("Could not find box for %s" % outletname)
