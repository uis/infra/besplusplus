from __future__ import print_function, unicode_literals

# General structure of content negotiation:
#
# Level 1: Given an incoming request, look at its headers and on the
# basis of some of them, choose which of several views to dispatch to.
# Add "Vary" headers to indicate which headers you looked at
#
# Level 2: Add a "Content-Location" header indicating a URL that will
# always go to the view that we chose.
#
# Level 3: Do this following the conventional procudure looking at
# "Accept", "Accept-Language", and "Accept-Encoding" headers.

from django.views.generic import View
from django.utils.cache import patch_vary_headers
from django.urls import resolve, reverse, NoReverseMatch

from ..http.accept import Accept, MediaType, InvalidAccept

class MultiView(View):
    """
    This is a class-based view to support proactive content negotiation
    as described in RFC 7231, which it does by dispatching to a different
    view for each representation of the target resource.  This is appropriate
    for negotiation based on media type, but maybe less so for other
    parameters.

    Users are expected to subclass this class and add a "negotiate_content"
    method.  When called with no arguments, this should look through the
    current request (in self.request) and work out what representation
    to return to the user-agent.  negotiate_content should return either
    a view function that will generate that representation or the name
    of a URL pattern that uses that view.  The latter is preferred, since
    it allows MultiView to set an appropriate Content-Location header.
    negotiate_content should call the "vary_on" method for any headers
    that it uses to make its decision (usually "Accept", but could
    plausibly be others).

    """
    def get(self, request, *args, **kwargs):
        self.vary_headers = set()
        view = self.negotiate_content()
        try:
            content_location = reverse(view, args=args, kwargs=kwargs)
            # If we were passed a URL pattern name, work out what view
            # it corresponds to.  Note that we don't bother re-extracting
            # args or kwargs, since they shouldn't have changed.
            if not callable(view):
                view = resolve(content_location).func
        except NoReverseMatch:
            content_location = None
        response = view(request, *args, **kwargs)
        patch_vary_headers(response, self.vary_headers)
        # Allow sub-views to set a more specific content-location.
        if content_location != None and 'Content-Location' not in response:
            response['Content-Location'] = content_location
        return response
    def vary_on(self, *headers):
        self.vary_headers.update(headers)
    def negotiate_content(self):
        """
        Default content-negotiator.  Looks in self.media_types for a
        (media-type, view or url pattern name) pairs, and
        picks the one the client prefers according to RFC 7231 5.3.2.
        """
        try:
            accept = Accept.from_string(
                self.request.META.get('HTTP_ACCEPT', ''))
            self.vary_on("accept")
            return sorted(self.media_types,
                key=lambda mt:-accept.q(MediaType.from_string(mt[0])))[0][1]
        except InvalidAccept:
            # Client supplied a bad "Accept" header.  Ignore it.
            return self.media_types[0][1]
