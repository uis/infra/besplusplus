from __future__ import print_function, unicode_literals

import json

from django.conf import settings
from django.urls import reverse
from django.http import HttpResponse
from django.views import generic

from ..models import Resource
from .resource import ResourceMixin

jldcontext = {
            '@vocab':      settings.BESPLUSPLUS_BASE_URI,
            '@base':       settings.BESPLUSPLUS_BASE_URI,
            'label':       'http://www.w3.org/2000/01/rdf-schema#label',
            'comment':     'http://www.w3.org/2000/01/rdf-schema#comment',
            'subclass-of': 'http://www.w3.org/2000/01/rdf-schema#subClassOf',
            'class':       'http://www.w3.org/2000/01/rdf-schema#Class',
            'property':    'http://www.w3.org/2000/01/rdf-schema#Property',
            'alias': { '@id': 'http://www.w3.org/2002/07/owl#sameAs',
                       '@type': '@id' },
            }

def jsonld_for_resource(r, reverse=True):
    j = { '@id': r.id }
    def val(x):
        if isinstance(x, Resource):
            return { '@id': x.id }
        return x
    def addkv(container, key, value):
        values = container.setdefault(key, [])
        if value not in values:
            values.append(value)
    for p in r.properties_fwd.all().select_related('object_res'):
        if p.predicate_id == 'type':
            addkv(j, '@type', p.object.id)
        else:
            addkv(j, p.predicate_id, val(p.object))
    if reverse:
        j['@reverse'] = { }
        for p in r.properties_rev.all().select_related('object_res'):
            addkv(j['@reverse'], p.predicate_id, val(p.subject))
    return j

class JSONLDView(ResourceMixin, generic.detail.BaseDetailView):
    """
    Class for rendering a Bes++ resource in JSON-LD format.  For more
    on JSON-LD, see <http://www.w3.org/TR/json-ld/>.  For more details
    of the Bes++ profile, see
    <https://wiki.cam.ac.uk/ucs/Bes%2B%2B_JSON-LD_API>.
    """
    def render_to_response(self, c):
        r = c['object']
        j = jsonld_for_resource(r)
        j['@context'] = jldcontext
        response = HttpResponse(json.dumps(j),
                                content_type="application/ld+json")
        # Suggest a sensible filename for saving (RFC 6266)
        response['Content-Disposition'] = (
                'inline; filename="%s.jsonld"' % (r.id,))
        # Allow AJAX access from other sites (http://www.w3.org/TR/cors/)
        response['Access-Control-Allow-Origin'] = '*'
        link = ('<%s>; rel=describes' %
            (reverse('id', kwargs={'pk': self.object.id}),))
        if 'Link' in response:
            response['Link'] += ", " + link
        else:
            response['Link'] = link
        return response

class JSONLDListView(generic.list.BaseListView):
    '''
    Class to render multiple resources as a JSON-LD list.
    '''
    def render_to_response(self, c):
        j = { '@context': jldcontext,
              '@graph': [ jsonld_for_resource(r) for r in c['object_list'] ] }
        response = HttpResponse(json.dumps(j),
                                content_type="application/ld+json")
        # Allow AJAX access from other sites (http://www.w3.org/TR/cors/)
        response['Access-Control-Allow-Origin'] = '*'
        return response

class JSONLDDump(generic.View):
    def get(self, request, *args, **kwargs):
        j = { '@context': jldcontext, '@graph': [ ] }
        for r in Resource.objects.all():
            j['@graph'].append(jsonld_for_resource(r, reverse=False))
        response = HttpResponse(json.dumps(j),
                                content_type="application/ld+json")
        # Suggest a sensible filename for saving (RFC 6266)
        response['Content-Disposition'] = 'inline; filename="dump.jsonld"'
        # Allow AJAX access from other sites (http://www.w3.org/TR/cors/)
        response['Access-Control-Allow-Origin'] = '*'
        return response
