from __future__ import print_function, unicode_literals

import yaml
import re
from django.utils.six import BytesIO
from zipfile import ZipFile

from django.http import HttpResponse
from django.utils.six import text_type
from django.views import generic

from ..models import Resource, PQ, RPQ

def geninfra(qs):
    """
    Generates the structures to be serialised to YAML to transfer
    information about the PDUs referenced by the supplied queryset
    into the infrastructure network database.
    """
    result = { 'pdus': { }, 'boxes': { } }
    for pdu in qs:
        pdulabels = pdu.prop('label')
        if not pdulabels: continue # Can't output an unlabelled PDU
        pdulabel = pdulabels[0]
        result['pdus'][pdulabel] = { }
        # XXX doesn't handle subclasses of <pdu-outlet>
        for outlet in (Resource.objects.filter(RPQ(incorporates=pdu))
                       .filter(PQ(type=Resource('pdu-outlet')))):
            outletnums = outlet.prop('label')
            if not outletnums: continue
            boxlabel = None
            fulllabel = None
            boxes = outlet.prop('powers')
            if boxes:
                box = boxes[0]
                fulllabel = text_type(box)
                # We've found the thing plugged into the outlet, but it might
                # be a PSU rather than the box itself.  If the target is
                # incorporated in something else, follow the link.
                if box.rprop('incorporates'):
                    box = box.rprop('incorporates')[0]
                # There are also special cases for switches with separate 2nd PSUs
                for p in box.rprop('powers'):
                    if p.has_type('eps-c'):
                        fulllabel = text_type(box) + ":ips"
                # ... and for those PSUs themselves.
                if box.has_type('eps-c') and box.prop('powers'):
                    box = box.prop('powers')[0]
                    fulllabel = text_type(box) + ":eps"
                boxlabel = text_type(box)
                sysadmins = [s for s in box.prop('sysadmin')
                             if re.match(r'^\d+$', s)]
            else: 
                deflabels = pdu.prop("infra-default-label")
                if deflabels:
                    boxlabel = deflabels[0]
                    fulllabel = boxlabel
                sysadmins = pdu.prop("infra-default-sysadmin")
            result['pdus'][pdulabel][int(outletnums[0])] = fulllabel
            # Only output boxes that have both sane names and sysadmins.
            if boxlabel and ':' not in boxlabel and sysadmins:
                result['boxes'].setdefault(boxlabel, { })
                result['boxes'][boxlabel]['sysadmins'] = (
                    sysadmins[0])
    return result

class DumpPDUs(generic.View):
    def get(self, request, *args, **kwargs):
        # XXX Support subclasses of <pdu>
        base = Resource.objects.filter(PQ(type=Resource('pdu')))
        if 'gateway' in request.GET:
            base = base.filter(PQ(infra_gateway=
                                  Resource(request.GET['gateway'])))
        return HttpResponse(yaml.safe_dump(geninfra(base)['pdus'],
                                           default_flow_style=False),
                            content_type="text/x.yaml")


class DumpInfra(generic.View):
    """
    Dump a Zip file suitable for unpacking into /etc/infra
    """
    def get(self, request, *args, **kwargs):
        # XXX Support subclasses of <pdu>
        base = Resource.objects.filter(PQ(type=Resource('pdu')))
        if 'gateway' in request.GET:
            base = base.filter(PQ(infra_gateway=
                                  Resource(request.GET['gateway'])))
        infra = geninfra(base)
        zipfile = BytesIO()
        zip = ZipFile(zipfile, 'w')
        for key, val in infra.items():
            zip.writestr(key, yaml.safe_dump(val, default_flow_style=False))
        zip.close()
        return HttpResponse(zipfile.getvalue(), content_type='application/zip')
