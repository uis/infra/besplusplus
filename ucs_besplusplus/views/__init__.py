from __future__ import print_function, unicode_literals

from django.urls import reverse
from django.contrib import messages
from django.db.models import Q
from django.views import generic
from django.http import Http404, HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404

from ..models import Resource, Property, HistoryRecord, PQ, RPQ, order_by_prop
from ..directory import groups_for_user, DirectoryException

from .resource import ResourceMixin, DetailView, ResourceMV
from .edit import EditView, CreateView
from .authmixins import LoginRequiredMixin, StaffOnlyMixin
from .jsonld import JSONLDView, JSONLDDump
from .history import HistoryView, ResourceHistoryView
from .infra import DumpPDUs, DumpInfra
from .search import SearchMV, SearchView, CompatSearchView, JSONLDSearchView
from .report import SoulsbyReport

class IndexView(generic.TemplateView):
    template_name="ucs_besplusplus/index.html"
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['history'] = HistoryRecord.objects.all().order_by('-when')[:5]
        return context

class MineView(LoginRequiredMixin, generic.ListView):
    def get_queryset(self):
        if 'fakeuser' in self.request.GET:
            username = self.request.GET['fakeuser']
        else:
            username = self.request.user.username
        q = PQ(sysadmin=username) | PQ(vmware_user='PWFAD\\' + username)
        try:
            for group in groups_for_user(username):
                q |= PQ(sysadmin=group)
        except Exception as e:
            messages.warning(self.request,
                             "Group information not available: " + str(e))
        return (Resource.objects.filter(q).extra(**order_by_prop('label'))
                .distinct())
    
from django.core import serializers

class DumpYAML(generic.View):
    def get(self, request, *args, **kwargs):
        return HttpResponse(serializers.serialize("yaml",
                 list(Property.objects.all()) + list(Resource.objects.all())),
                content_type="text/x.yaml")

class SeeOtherView(generic.RedirectView):
    def get(self, request, *args, **kwargs):
        response = super(SeeOtherView, self).get(request, *args, **kwargs)
        response.status_code = 303
        return response

import re
# These are primarily to avoid producing syntactically invalid
# configuration files, and to prevent errors in one host from
# affecting others.
def is_ipv4addr(x):
    return bool(re.match(r"^\d+\.\d+\.\d+\.\d+$", x))

def is_macaddr(x):
    return bool(re.match(r"^(?:[0-9a-f]{2}:){5}[0-9a-f]{2}$", x))

def is_fqdn(x):
    return bool(re.match(r"^[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*$", x))

def sanitise_label(x):
    m = re.match(r"^[ -~]*", x)
    return m.group(0)

class DumpDHCPConf(generic.View):
    def get(self, request, *args, **kwargs):
        base = Resource.objects.all()
        if 'dhcp-server' in request.GET:
            base = base.filter(RPQ(dhcp_server_for=
                                   Resource(request.GET['dhcp-server'])))
        ret = ""
        for target in base:
            macs = target.prop('mac-address')
            ipv4s = target.prop('ipv4-address')
            fqdns = target.prop('fqdn')
            # Skip any complicated case
            if len(macs) != 1 or len(fqdns) > 1: continue
            if (not all(map(is_ipv4addr, ipv4s)) or
                not all(map(is_macaddr, macs)) or
                not all(map(is_fqdn, fqdns))):
                continue
            ret += "# %s\n" % (sanitise_label(str(target)),)
            hostname = target.id
            if len(fqdns) == 1:
                hostname = fqdns[0]
            ret += "host %s {\n" % (hostname,)
            for ip in ipv4s:
                if is_ipv4addr(ip):
                    ret += "\tfixed-address %s;\n" % (ip,)
            ret += "\thardware ethernet %s;\n" % (macs[0],)
            ret += "}\n"
        return HttpResponse(ret, content_type="text/plain")

class DumpHosts(generic.View):
    def get(self, request, *args, **kwargs):
        base = Resource.objects.all()
        if 'dhcp-server' in request.GET:
            base = base.filter(RPQ(dhcp_server_for=
                                   Resource(request.GET['dhcp-server'])))
        ret = ""
        for target in base:
            ipv4s = target.prop('ipv4-address')
            fqdns = target.prop('fqdn')
            # Skip any complicated case
            if (not all(map(is_ipv4addr, ipv4s)) or
                not all(map(is_fqdn, fqdns))):
                continue
            for ipv4 in ipv4s:
                ret += "%s\t%s\n" % (ipv4, ' '.join(fqdns))
        return HttpResponse(ret, content_type="text/plain")
