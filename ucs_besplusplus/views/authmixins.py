from __future__ import print_function, unicode_literals

from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator

# This recipe comes from
# <https://docs.djangoproject.com/en/1.6/topics/class-based-views/intro/
#  #decorating-the-class>

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)

def staff_only(user):
    # Don't send user back to login prompt if the problem is that they're
    # not authorised.
    if user.is_authenticated() and not user.is_staff:
        raise PermissionDenied
    return user.is_staff

class StaffOnlyMixin(object):
    @method_decorator(user_passes_test(staff_only))
    def dispatch(self, *args, **kwargs):
        return super(StaffOnlyMixin, self).dispatch(*args, **kwargs)
