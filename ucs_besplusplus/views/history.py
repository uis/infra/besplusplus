from __future__ import print_function, unicode_literals

from django.views import generic
from django.db.models import Q
import logging
logger = logging.getLogger(__name__)

from ..models import HistoryRecord

class HistoryView(generic.ListView):
    paginate_by = 100
    model = HistoryRecord
    template_name = "ucs_besplusplus/history.html"

class ResourceHistoryView(HistoryView):
    def get_queryset(self):
        logger.info("Hello from ResourceHistoryView")
        target = self.kwargs['pk']
        return (HistoryRecord.objects
            .filter(Q(subject_id=target) | Q(object_res_id=target)))
