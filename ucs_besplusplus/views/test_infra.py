from __future__ import unicode_literals

from django.test import TestCase

from ..models import Resource, PQ, RPQ
from .infra import geninfra

R = Resource

def ensure_resource(id=None, **props):
    if id == None:
        r = Resource()
    else:
        r = Resource(id)
    r.save()
    for k, v in props.items():
        k = k.replace('_', '-')
        r.addprop(k, v)
    return r

def createpdu(l, gw):
    pdu = ensure_resource(None, type=R('pdu'), label=l,
                            fqdn=l + ".infra", infra_gateway=gw)
    o = [ ]
    for on in range(1, 21):
        o = ensure_resource(type=R('pdu-outlet'))
        o.addrprop('incorporates', pdu)
        o.addprop('label', str(on))
    return pdu

def connecttopdu(pdu, on, dev):
    o = Resource.objects.filter(RPQ(incorporates=pdu)).get(PQ(label=str(on)))
    o.addprop('powers', dev)

def setup():
    for p in ['label', 'incorporates', 'sysadmin', 'fqdn', 'infra-gateway',
              'powers']:
        ensure_resource(p, type=R('property'))
    for c in ['pdu', 'pdu-outlet', 'box', 'psu', 'eps-c']:
        ensure_resource(c, type=R('class'))
    gw = ensure_resource(None, label="301.infra.csi.cam.ac.uk")
    pdu_o = createpdu("pdu-301-a1-o", gw)
    pdu_k = createpdu("pdu-301-a1-k", gw)
    # simple box
    box1 = ensure_resource(None, label="box1", type=R('box'))
    connecttopdu(pdu_o, 1, box1)
    connecttopdu(pdu_k, 1, box1)
    box2 = ensure_resource(label="box2", type=R('box'))
    box2ps1 = ensure_resource(label="ps1")
    box2.addprop('incorporates', box2ps1)
    box2ps2 = ensure_resource(label="ps2")
    box2.addprop('incorporates', box2ps2)
    connecttopdu(pdu_o, 2, box2ps1)
    connecttopdu(pdu_k, 3, box2ps2)
    box3 = ensure_resource(None, label="box3", type=R('box'))
    connecttopdu(pdu_o, 7, box3)
    box1.addprop('sysadmin', '123456')
    box2.addprop('sysadmin', '123457')
    sw = ensure_resource(label="sw0", type=R('box'))
    eps = ensure_resource(label="sw0-eps", type=R('eps-c'), powers=sw)
    connecttopdu(pdu_o, 8, sw)
    connecttopdu(pdu_k, 8, eps)
    ips =  ensure_resource(label="ips", type=R('psu'))
    sw = ensure_resource(label="sw1", type=R('box'), incorporates=ips,
                         sysadmin="123456")
    eps = ensure_resource(label="sw1:rps", type=R('box'), powers=sw,
                          sysadmin="123456")
    connecttopdu(pdu_o, 9, ips)
    connecttopdu(pdu_k, 9, eps)
    return gw

class InfraTests(TestCase):
    maxDiff=None
    def test_infra(self):
        gw = setup()
        self.assertEqual(geninfra(Resource.objects.filter(PQ(infra_gateway=gw))),
            { 'boxes': {
                'box1': { 'sysadmins': '123456' },
                'box2': { 'sysadmins': '123457' },
                'sw1': { 'sysadmins': '123456' },
                },
              'pdus': {
                'pdu-301-a1-o': {
                    1: 'box1',
                    2: 'box2:ps1',
                    3: None,
                    4: None,
                    5: None,
                    6: None,
                    7: 'box3',
                    8: 'sw0:ips',
                    9: 'sw1:ips',
                    10: None,
                    11: None,
                    12: None,
                    13: None,
                    14: None,
                    15: None,
                    16: None,
                    17: None,
                    18: None,
                    19: None,
                    20: None,
                },
                'pdu-301-a1-k': {
                    1: 'box1',
                    2: None,
                    3: 'box2:ps2',
                    4: None,
                    5: None,
                    6: None,
                    7: None,
                    8: 'sw0:eps',
                    9: 'sw1:rps',
                    10: None,
                    11: None,
                    12: None,
                    13: None,
                    14: None,
                    15: None,
                    16: None,
                    17: None,
                    18: None,
                    19: None,
                    20: None,
                },
              }
            })
