from __future__ import print_function, unicode_literals

from django.views import generic

from ..models import Resource, PQ, RPQ, order_by_intprop

class SoulsbyReport(generic.ListView):
    def get_queryset(self, query=None):
        sby = Resource('d54a0f15-d74d-446d-9d0a-dc812c7306c5')
        racks = Resource.objects.filter(RPQ(contains=sby))
        boxes = (Resource.objects.filter(RPQ(contains__in=racks))
                 .filter(PQ(misd_inv__contains=''))
                 .extra(**order_by_intprop('misd-inv')))
        return boxes
    template_name = "ucs_besplusplus/sby.html"
