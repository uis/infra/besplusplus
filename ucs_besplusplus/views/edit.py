from __future__ import print_function, unicode_literals

from django.core.exceptions import ValidationError
from django.views import generic
from django import forms
from django.utils.encoding import python_2_unicode_compatible
from django.utils.safestring import mark_safe

import itertools

from ..models import Resource, Property, PQ, RPQ, order_by_prop
from .resource import ResourceMixin
from .authmixins import StaffOnlyMixin

# This code is a mess, largely because it's trying to do a complicated
# job using parts of Django that are less well-designed than is usual.
# The structure that it's trying to create can be described by:
#
# For some resource R:
#   For each forward property P of R:
#     An InlineFormSet containing:
#       For each existing value of P:
#         A Form containing:
#           Label identifying P
#           Hidden field "predicate"
#           Visible field "object_res" or "object_lit" depending on P
#           Checkbox to delete this (R, P, V)
#       Optionally:
#         A Form for creating a new Property containing:
#           Label identifying P
#           Hidden field "predicate"
#           Visible field "object_res" or "object_lit" depending on P
#   ... and then the equivalent for all reverse properties.
#
# All of this needs to be stuck into a Web page and made friendly.

class repeat(itertools.repeat):
    def __init__(self, o, times=None):
        self.__times = times
        self.__o = o
    def __nonzero__(self):
        return self.__times != 0
    def __len__(self):
        if self.__times != None:
            return self.__times
        raise TypeError("indefinite repeat has no length")
    def __getitem__(self, key):
        return self.__o

class PowerWidget(forms.MultiWidget):
    def __init__(self, *args, **kwargs):
        widgets = (forms.TextInput(), forms.TextInput())
        super(PowerWidget, self).__init__(widgets, *args, **kwargs)
    def decompress(self, value):
        if value == None:
            return [None, None]
        else:
            outlet = Resource(value)
            if outlet.rprop('incorporates'):
                label = outlet.prop('label')[0]
                return [outlet.rprop('incorporates')[0].id, label]
            return [outlet.id, '']

class PowerField(forms.MultiValueField):
    def __init__(self, *args, **kwargs):
        self.widget = PowerWidget()
        fields = (forms.ModelChoiceField(
            Resource.objects.filter(PQ(type=Resource('pdu'))).distinct(),
            widget=self.widget.widgets[0]),
            forms.CharField(widget=self.widget.widgets[1]))
        super(PowerField, self).__init__(fields, *args, **kwargs)
    def compress(self, data_list):
        pdu = data_list[0]
        if data_list[1] == '':
            return pdu
        os = (Resource.objects.filter(RPQ(incorporates=pdu))
              .filter(PQ(label=data_list[1])))
        if os:
            o = os[0]
        else:
            raise ValidationError("%s has no outlet labelled %s" %
                                  (pdu, data_list[1]))
        return o

class LitPropForm(forms.models.ModelForm):
    class Meta:
        model = Property
        fields = ('predicate', 'object_lit')

class CommentForm(LitPropForm):
    object_lit = forms.CharField(widget=forms.Textarea, label="comment")

class ResPropForm(forms.models.ModelForm):
    class Meta:
        model = Property
        fields = ('predicate', 'object_res')

class TypeForm(ResPropForm):
    object_res = forms.models.ModelChoiceField(
        Resource.objects.filter(PQ(type=Resource('class'))).order_by('id')
                                                           .distinct(),
        label="type")

def formsetclass_for_prop(p):
    formclass = LitPropForm
    if p == 'comment':
        formclass = CommentForm
    if p == 'type':
        formclass = TypeForm
    return forms.models.inlineformset_factory(
        Resource, Property, fk_name='subject', max_num=1, extra=1,
        form=formclass,
        labels={'object_lit': p, 'object_res': p},
        widgets={'predicate': forms.HiddenInput})

class RevPropForm(forms.models.ModelForm):
    class Meta:
        model = Property
        fields = ('predicate', 'subject')

class RevContainsForm(RevPropForm):
    subject = forms.models.ModelChoiceField(
        Resource.objects.filter(PQ(type=Resource('rack')))
          .extra(**order_by_prop('label')).distinct(),
        label="Location")

class RevPowersForm(RevPropForm):
    subject = PowerField(label="Powered by")

def formsetclass_for_rprop(p):
    formclass = RevPropForm
    max_num = 1
    extra = 1
    if p == 'contains':
        formclass = RevContainsForm
    if p == 'powers':
        formclass = RevPowersForm
        extra = 3
        max_num = 3
    return forms.models.inlineformset_factory(
        Resource, Property, fk_name='object_res', max_num=max_num, extra=extra,
        form=formclass,
        labels={'subject': 'reverse ' + p},
        widgets={'predicate': forms.HiddenInput})

@python_2_unicode_compatible
class ResourceForm(object):
    # A ResourceForm is a collection of Formsets, one for each interesting
    # property of a resource, and wrapped up in a way that simulates a
    # Django Form.
    def __init__(self, initial, prefix, instance, files=None, data=None):
        def pfs(p):
            return formsetclass_for_prop(p)(
                data=data, instance=instance, save_as_new=save_as_new,
                initial=repeat({'predicate': p}),
                queryset=Property.objects.filter(predicate=p),
                prefix='fwd-' + p.replace('-', '_'))
        def rpfs(p):
            return formsetclass_for_rprop(p)(
                data=data, instance=instance, save_as_new=save_as_new,
                initial=repeat({'predicate': p}),
                queryset=Property.objects.filter(predicate=p),
                prefix='rev-'+p.replace('-', '_'))
        self.initial = initial
        self.prefix = prefix
        save_as_new = False
        if instance == None and data != None:
            # A form has been submitted that would create a new resource.
            save_as_new = True
            instance = Resource() # the new resource
            # save_as_new causes "data" to be written to, so we copy it to make
            # it writable.  <https://code.djangoproject.com/ticket/21955>
            data = data.copy()
        self.instance = instance
        self.formsets = [pfs('type'), pfs('label'),
                         pfs('fqdn'),
                         pfs('ucs-inv'),
                         pfs('serial-number'),
                         pfs('sysadmin'),
                         pfs('u-size'),
                         rpfs('contains'), pfs('u-position'),
                         rpfs('powers'),
                         pfs('comment'),
                         ]
    def __str__(self):
        return self.as_table()
    def as_table(self):
        return mark_safe(' '.join([formset.as_table()
                                   for formset in self.formsets]))
    def is_valid(self):
        return all([f.is_valid() for f in self.formsets])
    @property
    def errors(self):
        e = [ ]
        for f in self.formsets:
            e.append(f.errors)
        return e
    def total_error_count(self):
        e = 0
        for f in self.formsets:
            e += f.total_error_count()
        return e
    def save(self):
        self.instance.save() # in case it needs to be created
        for f in self.formsets:
            f.save()
        return self.instance

class EditView(ResourceMixin, StaffOnlyMixin, generic.UpdateView):
    form_class = ResourceForm

class CreateView(ResourceMixin, StaffOnlyMixin, generic.CreateView):
    form_class = ResourceForm
