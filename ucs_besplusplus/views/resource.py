from __future__ import print_function, unicode_literals

from django.urls import reverse
from django.db.models import Q
from django.views import generic
from django.http import Http404, HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404

import urllib.parse

from ..models import Resource, PQ, HistoryRecord

from ..templatetags.ucs_besplusplus import (rack_power_url, pdu_power_url,
                                            box_power_url)

from .authmixins import LoginRequiredMixin
from .negotiation import MultiView

class ResourceMixin(generic.detail.SingleObjectMixin):
    """
    Class for views representing a resource.  Generates redirects on
    GET as necessary.
    """
    model = Resource
    def get(self, request, *args, **kwargs):
        try:
            return super(ResourceMixin, self).get(request, *args, **kwargs)
        except Http404:
            # Before returning 404, try looking for resources with the
            # requested id as an alias.
            try:
                r = get_object_or_404(Resource, PQ(alias=kwargs['pk']))
                return HttpResponsePermanentRedirect(
                    reverse(self.request.resolver_match.view_name,
                            kwargs={'pk': r.id}))
            except Resource.MultipleObjectsReturned:
                # This is probably not the most sensible way to handle this.
                return HttpResponse(
                    "Database disaster: multiple resources with this alias",
                    status=300)

class ExternalLink(object):
    def __init__(self, label, url):
        self.label = label
        self.url = url

class DetailView(ResourceMixin, LoginRequiredMixin, generic.DetailView):
    def get(self, request, *args, **kwargs):
        response = super(DetailView, self).get(request, *args, **kwargs)
        # Only add a "Link" header to successful responses.
        if 200 <= response.status_code <= 299:
            link = ('<%s>; rel=alternate; type=application/ld+json,'
                    '<%s>; rel=describes' %
                (reverse('jsonld', kwargs={'pk': self.object.id}),
                 reverse('id', kwargs={'pk': self.object.id})))
            if 'Link' in response:
                response['Link'] += ", " + link
            else:
                response['Link'] = link
        return response
    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['related_links'] = []
        self.add_power_context(context)
        self.add_dell_link(context)
        self.add_map_link(context)
        context['history'] = (HistoryRecord.objects
            .filter(Q(subject=self.object) | Q(object_res=self.object))
            .order_by('-when')[:5])
        return context
    def power_url(self, resource):
        # Racks and PDUs have special power URLs
        if resource.has_type('rack'):
            return rack_power_url(resource)
        if resource.has_type('pdu'):
            return pdu_power_url(resource)
        for src in resource.rprop('powers'):
            if src.has_type('pdu-outlet'):
                return box_power_url(resource)
    def add_link(self, context, label, url):
        context['related_links'].append(ExternalLink(label, url))
    def add_power_context(self, context):
        url = self.power_url(self.object)
        if url:
            self.add_link(context, 'Power monitoring', url)
    def add_dell_link(self, context):
        if self.object.has_type('dell-box'):
            for sn in self.object.prop('serial-number'):
                # XXX this URL is probably fragile
                url = ('http://www.dell.com/support/home/uk/en/ukdhs1/' +
                       'product-support/servicetag/%s') % urllib.parse.quote(sn)
                self.add_link(context, 'Dell support', url)
    def add_map_link(self, context):
        # At most 25 refs can be provided to the map.
        refs = self.object.prop('map-ref')[:25]
        if refs:
            url = ('http://map.cam.ac.uk?ref=%s' %
                   urllib.parse.quote('|'.join(refs)))
            self.add_link(context, 'University map', url)

from .jsonld import JSONLDView

class ResourceMV(MultiView):
    media_types = (
        ('text/html; charset=utf-8', 'resource.html'),
        ('application/ld+json', 'jsonld'),
        ('application/json', 'jsonld'))
