from __future__ import print_function, unicode_literals

from itertools import chain
import re
from django.utils.six.moves.urllib_parse import urlencode

from django.urls import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views import generic

from .jsonld import JSONLDListView
from .negotiation import MultiView
from ..models import Resource, PQ, RPQ, order_by_prop

class MaybeRedirect(generic.list.BaseListView):
    """
    Modification of BaseListView that causes a redirect to the single
    resource in the queryset if there's precisely one.
    """
    singular_pattern_name = None
    def render_to_response(self, c):
        if len(c['object_list']) == 1:
            if self.singular_pattern_name == None:
                dest = c['object_list'][0].get_absolute_url()
            else:
                dest = reverse(self.singular_pattern_name,
                               kwargs=dict(pk=c['object_list'][0].pk))
            return HttpResponseRedirect(dest)
        return super(MaybeRedirect, self).render_to_response(c)

class Searcher(generic.list.BaseListView):
    def get_queryset(self, query=None):
        qs = Resource.objects.all()
        for key, values in self.request.GET.lists():
            for value in values:
                def lookslikeprop(p):
                    return (re.match(r"^[A-Za-z-][A-Za-z0-9-]*$", p) and
                            "--" not in p)
                if key.startswith("*"):
                    # Keys that start with '*' are to be ignored
                    pass
                elif key == ".query":
                    # Human-friendly search field
                    qs = qs.filter(
                        Q(properties_fwd__object_lit__icontains=value.strip()) |
                        Q(id=value.strip()))
                else:
                    terms = key.split('.')
                    if (len(terms) == 3 and
                        terms[0] == '@reverse' and terms[2] == '@id' and
                        lookslikeprop(terms[1])):
                        # @reverse.foo.@id=bar
                        qs = qs.filter(RPQ(**{ terms[1]: Resource(value) }))
                    elif (len(terms) == 3 and
                        terms[0] == '@reverse' and terms[2] == '@exists' and
                        lookslikeprop(terms[1])):
                        # @reverse.foo.@exists=
                        qs = qs.filter(Q(properties_rev__predicate=terms[1]))
                    elif (len(terms) == 2 and
                        terms[1] == '@id' and lookslikeprop(terms[0])):
                        # foo.@id=bar
                        qs = qs.filter(PQ(**{ terms[0]: Resource(value) }))
                    elif (len(terms) == 2 and
                        terms[1] == '@exists' and lookslikeprop(terms[0])):
                        # foo.@exists=
                        qs = qs.filter(Q(properties_fwd__predicate=terms[0]))
                    elif terms == ['@type']:
                        # @type=bar
                        qs = qs.filter(PQ(type=Resource(value)))
                    elif (len(terms) == 1 and lookslikeprop(terms[0])):
                        # foo=bar
                        qs = qs.filter(PQ(**{ terms[0]: value }))
        return qs.extra(**order_by_prop('label')).distinct()

class SearchView(MaybeRedirect, Searcher, generic.ListView):
    pass

class JSONLDSearchView(MaybeRedirect, Searcher, JSONLDListView):
    singular_pattern_name = 'jsonld'

class SearchMV(MultiView):
    media_types = (
        ('text/html; charset=utf-8', 'search.html'),
        ('application/ld+json', 'search.jsonld'),
        ('application/json', 'search.jsonld'))
    
class CompatSearchView(generic.RedirectView):
    """
    Generate a redirect to convert old-style search (single argument
    called "query") into new-style (".query" for free text, "query"
    would look for a property called "query").
    """
    pattern_name='search'
    permanent=True
    def get_redirect_url(self, *args, **kwargs):
        url = super(CompatSearchView, self).get_redirect_url(*args, **kwargs)
        if "query" in self.request.GET:
            qstr = urlencode({".query": self.request.GET["query"]})
            url = "%s?%s" % (url, qstr)
        return url
