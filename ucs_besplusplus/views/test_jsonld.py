from __future__ import unicode_literals

from django.test import TestCase

from ..models import Resource, Property
from ..importer import simplejson as sj
from . import jsonld as jldview

class JSONLDTests(TestCase):
    def test_0simple(self):
        sj.do_import({'@graph': [{'@id': 'foo'},
                                 {'@id': 'bar', 'powers': ['foo',
                                                           {'@id': 'foo'},
                                                           {'@id': 'bar'}]},
                                 {'@id': 'baz'}]})
        # Add a duplicate property
        Property.objects.create(
            subject_id='bar', predicate_id='powers', object_res_id='foo',
            source_id='foo')
        j = jldview.jsonld_for_resource(Resource("bar"))
        self.assertIn('powers', j)
        self.assertEqual(len(j['powers']), 3)
        self.assertIn('@reverse', j)
        self.assertIn('powers', j['@reverse'])
        self.assertEqual(j['@reverse']['powers'], [{'@id': 'bar'}])
