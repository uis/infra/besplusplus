from __future__ import unicode_literals

import json

from django.urls import reverse
from django.test import TestCase, Client

try:
    # Python 2.6 compatibility
    from django.utils.unittest import expectedFailure
except ImportError:
    from unittest import expectedFailure

from ..models import Resource
from ..importer import simplejson as sj

class SearchTests(TestCase):
    def setUp(self):
        sj.do_import ([{'@id': 'a000', '@type': 'thing', 'label': 'foo'},
                       {'@id': 'a001', '@type': 'entity', 'label': 'bar'},
                       {'@id': 'a002', 'serial-number': '123',
                        'label': 'abc'},
                       {'@id': 'a003', 'serial-number': '123',
                        'label': 'def'},
                       {'@id': 'a004', 'serial-number': '234',
                        'label': 'def', 'contains': {'@id': 'a000'}},
                       {'@id': 'a005', 'label': ['foo', 'bar']}])
        self.c = Client(HTTP_ACCEPT="application/ld+json")
    def extract_ids(self, response):
        j = json.loads(response.content)
        if '@graph' not in j:
            j = {'@graph': [j]}
        return set([o['@id'] for o in j['@graph']])
    def assert_searchresult(self, query, result):
        response = self.c.get(reverse('search.jsonld'), query,
                              follow=True)
        self.assertEqual(self.extract_ids(response), result)        
    def test_human(self):
        self.assert_searchresult({'.query': 'foo'}, set(['a000', 'a005']))
        self.assert_searchresult({'.query': 'bar'}, set(['a001', 'a005']))
    def test_label(self):
        self.assert_searchresult({'label': 'foo'}, set(['a000', 'a005']))
        self.assert_searchresult({'label': 'def'}, set(['a003', 'a004']))
    def test_ignore(self):
        self.assert_searchresult({'label': 'foo', '*ignore': 'murlf'},
                                 set(['a000', 'a005']))
    def test_conjunction(self):
        self.assert_searchresult({'serial-number': '123', 'label': 'def'},
                                 set(['a003']))
    def test_multiple(self):
        self.assert_searchresult({'label': ['foo', 'bar']}, set(['a005']))
    def test_id_not_literal(self):
        self.assert_searchresult({'label.@id': 'foo'}, set([]))
    def test_id(self):
        self.assert_searchresult({'contains.@id': 'a000'}, set(['a004']))
    def test_reverse(self):
        self.assert_searchresult({'@reverse.contains.@id': 'a004'},
                                 set(['a000']))
    def test_exists(self):
        self.assert_searchresult({'serial-number.@exists': ''},
                                 set(['a002', 'a003', 'a004']))
    def test_reverse_exists(self):
        self.assert_searchresult({'@reverse.contains.@exists': ''},
                                 set(['a000']))
    def test_type(self):
        self.assert_searchresult({'@type': 'thing'}, set(['a000']))
