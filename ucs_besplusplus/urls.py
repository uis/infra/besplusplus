from __future__ import print_function, unicode_literals

from django.conf.urls import include, url
from django.views import generic

from . import views, admin

# Function for changing the canonical URL of a page.
# Bes++ has an unfortunate history of bad URL choices.
def redirect_to(pattern):
    return generic.RedirectView.as_view(
        pattern_name=pattern, permanent=True, query_string=True)

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^!admin/', admin.site.urls),
    url(r'^!edit/(?P<pk>.+)/$', views.EditView.as_view(), name='edit'),
    url(r'^!jsonld/(?P<pk>.+)/$', redirect_to('jsonld')),
    url(r'^!jsonld/(?P<pk>.+)$', redirect_to('jsonld')),
    url(r'^!search/$', views.CompatSearchView.as_view()),
    url(r'^!search$', views.SearchMV.as_view(), name='search'),
    url(r'^!search.html$', views.SearchView.as_view(), name='search.html'),
    url(r'^!search.json$', redirect_to('search.jsonld')),
    url(r'^!search.jsonld$', views.JSONLDSearchView.as_view(),
        name='search.jsonld'),
    url(r'^!create/$', views.CreateView.as_view(), name='create'),
    url(r'^!dumpyaml/$', views.DumpYAML.as_view(), name='dumpyaml'),
    url(r'^!dumppdus/$', views.DumpPDUs.as_view(), name='dumppdus'),
    url(r'^!dumpinfra/$', views.DumpInfra.as_view(), name='dumpinfra'),
    url(r'^!dumpdhcpconf/$', views.DumpDHCPConf.as_view(), name='dumpdhcpconf'),
    url(r'^!dumphosts/$', views.DumpHosts.as_view(), name='dumphosts'),
    url(r'^!dumpjsonld/$', views.JSONLDDump.as_view(), name='dumpjsonld'),
    url(r'^!sby$', views.SoulsbyReport.as_view(), name='sby'),
    url(r'^!mine/$', views.MineView.as_view(), name='mine'),
    url(r'^!history/$', views.HistoryView.as_view(), name='history'),
    url(r'^(?P<pk>.+)/!history$', views.ResourceHistoryView.as_view(),
        name="resource_history"),
    url(r'^(?P<pk>.+)/$', redirect_to('resource')),
    url(r'^(?P<pk>.+).html$', views.DetailView.as_view(),
        name='resource.html'),
    url(r'^(?P<pk>.+).json$', redirect_to('jsonld')),
    url(r'^(?P<pk>.+).jsonld$', views.JSONLDView.as_view(), name='jsonld'),
    url(r'^(?P<pk>.+).\*$', views.ResourceMV.as_view(), name='resource'),
    url(r'^(?P<pk>.+)$', views.SeeOtherView.as_view(pattern_name='resource'),
        name='id'),
    ]
