from __future__ import print_function, unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from pyparsing import (CaselessLiteral, Combine, Dict, Empty, Group, Literal,
                           Optional, ParseException, QuotedString, Suppress,
                           White, Word, ZeroOrMore, alphas, nums,
                           alphanums, dictOf, pyparsing_common)

OWS = ZeroOrMore(Literal(" ") | Literal("\t"))

tchars = "!#$%&'*+-.^_`|~" + alphanums

token = Word(tchars)

quoted_string = QuotedString(quoteChar='"', escChar="\\",
                                 convertWhitespaceEscapes=False)

# Many tokens in HTTP are case-insensitive.
itoken = token.copy().addParseAction(pyparsing_common.downcaseTokens)
type = itoken
subtype = itoken

# Separator for parameter lists
semi = OWS + Suppress(";") + OWS

def paramList(key):
    param = Group(semi + key + Suppress("=") + (token | quoted_string))
    params = ZeroOrMore(param)
    params.setParseAction(lambda toks: dict(toks.asList()))
    return params

# The special treatment of "*" is handled by the MediaRange constructor.
media_range = (type + Suppress("/") + subtype +
                   paramList(~CaselessLiteral("q=") + itoken))
media_range.setParseAction(lambda toks: MediaRange(*toks))

qvalue = Combine( Literal("0") + Optional(Word(".", nums, max=4)) |
                  Literal("1") + Optional(Word(".", "0",  max=4)) )
qvalue.setParseAction(lambda toks: float(toks[0]))

weight = semi + Suppress(CaselessLiteral("q=")) + qvalue

accept_ext = semi + itoken + Optional(Suppress("=") + (token ^ quoted_string))
# Accept extensions have no defined semantics, so ignore them.
accept_params = weight + Suppress(ZeroOrMore(accept_ext))

defaultq = Empty().setParseAction(lambda toks: 1.0)
accept1 = Group(media_range + (accept_params | defaultq))

# Comma-separated lists. RFC 7230 section 7.
def commalist(member):
    comma = OWS + Suppress(",") + OWS
    return Optional(member) + ZeroOrMore(comma + Optional(member))

accept = commalist(accept1)
accept.setParseAction(lambda toks: Accept(toks.asList()))

media_type = type + Suppress("/") + subtype + paramList(token)
media_type.setParseAction(lambda toks: MediaType(*toks))

class ParsedThing(object):
    @classmethod
    def from_string(cls, s):
        pr = cls.production.parseString(s, True)
        return pr[0]

@python_2_unicode_compatible
class MediaType(ParsedThing):
    """
    Class to represent a single HTTP media-type
    """
    production = media_type
    def __init__(self, type, subtype, params):
        self.type = type
        self.subtype = subtype
        self.params = params
    def __str__(self):
        return (self.type or "*") + "/" + (self.subtype or "*")


class InvalidAccept(Exception):
    pass

class MediaRange(ParsedThing):
    production = media_range
    def __init__(self, type, subtype, params):
        self.type = type
        self.subtype = subtype
        self.params = params or { }
        # Recognise special cases.
        if self.type == '*' and self.subtype == '*' and self.params == {}:
            self.type = None
            self.subtype = None
        elif self.subtype == '*' and self.params == {}:
            self.subtype = None
    def __str__(self):
        return (self.type or "*") + "/" + (self.subtype or "*")
    def __repr__(self):
        return ("MediaRange(" +
                ", ".join([repr(x) for x in
                           (self.type, self.subtype, self.params)]) + ")")
    def matches(self, mt):
        """
        Given a MediaType, return True if this media-range matches it.
        """
        if self.type != None and self.type != mt.type: return False
        if self.subtype != None and self.subtype != mt.subtype: return False
        # An media-range with a parameter only matches representations
        # with precisely the same parameter.  On the other hand, parameters
        # not mentioned in the media-range can be ignored.
        for k in self.params:
            if k not in mt.params: return False
            if self.params[k] != mt.params[k]: return False
        return True
    def specificity(self):
        """
        When several media-ranges in an Accept header-field match a
        media-type, the one with the highest specificity wins.
        """
        ret = len(self.params)
        if self.type != None: ret += 1
        if self.subtype != None: ret += 1
        return ret

class Accept(ParsedThing):
    production = accept
    @classmethod
    def from_string(cls, s):
        try:
            return super(Accept, cls).from_string(s)
        except ParseException as e:
            raise InvalidAccept(str(e))
    def __init__(self, field):
        self.possibilities = [{'media_range': x[0], 'qvalue': x[1]}
                                  for x in field]
        self.possibilities.sort(key=lambda x:x['media_range'].specificity(),
                                reverse=True)
    def q(self, mt):
        """
        Work out the q value for a given MediaType.
        """
        for p in self.possibilities:
            if p['media_range'].matches(mt):
                return p['qvalue']
        return 0.0
