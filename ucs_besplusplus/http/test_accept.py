from __future__ import print_function, unicode_literals

from .accept import Accept, MediaType, MediaRange, InvalidAccept

import unittest

class TestMediaType(unittest.TestCase):
    def test_creation(self):
        mt = MediaType.from_string("text/html; level=4; charset=utf-8")
        self.assertEqual((mt.type, mt.subtype, mt.params),
                         ("text", "html", { "level": "4", "charset": "utf-8"}))

class TestMediaRange(unittest.TestCase):
    def test_precedence(self):
        # Sample precedence order from RFC 7231
        self.assertEqual(MediaRange.from_string('text/plain;format=flowed')
                         .specificity(), 3)
        self.assertEqual(MediaRange.from_string('text/plain').specificity(), 2)
        self.assertEqual(MediaRange.from_string('text/*').specificity(), 1)
        self.assertEqual(MediaRange.from_string('*/*').specificity(), 0)
    def test_matching(self):
        tt = [('*/*', 'text/plain'),
              ('text/*', 'text/plain'),
              ('text/plain', 'text/plain'),
              ('text/plain', 'text/plain;charset=utf-8'),
              ('text/plain; format="flowed"', 'text/plain;format=flowed'),
            ]
        tf = [('text/html;level=1', 'text/html'),
              ('*/xml', 'application/xml'),
            ]
        for acc, ct in tt:
            self.assertTrue(MediaRange.from_string(acc)
                            .matches(MediaType.from_string(ct)))
        for acc, ct in tf:
            self.assertFalse(MediaRange.from_string(acc)
                             .matches(MediaType.from_string(ct)))
        
def mtfs(x): return MediaType.from_string(x)

class TestAccept(unittest.TestCase):
    def test_rfc7231_parse(self):
        """Make sure we can parse the examples from section 5.3.2"""
        a = Accept.from_string("audio/*; q=0.2, audio/basic")
        self.assertLess(a.q(mtfs('audio/mp3')), a.q(mtfs('audio/basic')))
        self.assertEqual(a.q(mtfs('text/html')), 0.0)
        Accept.from_string(
            "text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c")
        Accept.from_string("text/*, text/plain, text/plain;format=flowed, */*")
        Accept.from_string("text/*;q=0.3, text/html;q=0.7, text/html;level=1, "
               "text/html;level=2;q=0.4, */*;q=0.5")
    def test_rfc7231_full(self):
        """Check the table at the end of section 5.3.2"""
        a = Accept.from_string(
             "text/*;q=0.3, text/html;q=0.7, text/html;level=1, "
             "text/html;level=2;q=0.4, */*;q=0.5")
        self.assertEqual(a.q(mtfs('text/html;level=1')), 1  )
        self.assertEqual(a.q(mtfs('text/html')),         0.7)
        self.assertEqual(a.q(mtfs('text/plain')),        0.3)
        self.assertEqual(a.q(mtfs('image/jpeg')),        0.5)
        self.assertEqual(a.q(mtfs('text/html;level=2')), 0.4)
        self.assertEqual(a.q(mtfs('text/html;level=3')), 0.7)
    def test_quoted(self):
        """Check that we can at least parse quoted strings"""
        a = Accept.from_string(
            'application/ld+json; profile="http://example/foo,bar"')
        self.assertEqual(len(a.possibilities), 1)
    def test_realworld(self):
        # Safari 6.1.6 pages
        Accept.from_string(
            "text/html,application/xhtml+xml,application/xml;q=0.9,"
            "*/*;q=0.8")
        # Safari 6.1.6 stylesheets
        Accept.from_string("text/css,*/*;q=0.1")
        # Firefox 37.0 images
        Accept.from_string("image/png,image/*;q=0.8,*/*;q=0.5")
    def test_extracommas(self):
        a = Accept.from_string(",,text/html,,image/*,,")
        self.assertEqual(len(a.possibilities), 2)
    def test_caseless(self):
        a = Accept.from_string("text/html;Q=0.1")
        self.assertEqual(a.q(mtfs('text/html')), 0.1)
    def test_extensions(self):
        a = Accept.from_string("text/html;q=1;moo;level=7;q=0,*/*;q=0.5")
        self.assertEqual(a.q(mtfs('text/html')), 1.0)
    def test_errors(self):
        with self.assertRaises(InvalidAccept):
            Accept.from_string("moo")
        with self.assertRaises(InvalidAccept):
            Accept.from_string("text/html;moo")
        with self.assertRaises(InvalidAccept):
            Accept.from_string("this/that/theother")
        with self.assertRaises(InvalidAccept):
            Accept.from_string(",,,,.")
        with self.assertRaises(InvalidAccept):
            Accept.from_string("x/y;q=1.001")
        with self.assertRaises(InvalidAccept):
            Accept.from_string("*/*;q=0.0000")
                
if __name__ == '__main__':
    unittest.main()
