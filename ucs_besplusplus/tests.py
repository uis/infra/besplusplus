from __future__ import unicode_literals

from django.utils.six import StringIO, text_type

from django.test import TestCase
from django.test.utils import override_settings
from django.core.exceptions import ValidationError
from django.urls import reverse

import ucs_besplusplus.importer.simplejson as sj
from .models import Resource, Property, id_from_fqdn

class InitialDataTests(TestCase):
    def test_properties(self):
        # Predefined properties should all have NULL literal objects.
        self.assertEqual(Resource('type').properties_fwd
                         .get(predicate_id='type').object_lit, None)
        self.assertEqual(Resource('class').properties_fwd
                         .get(predicate_id='type').object_lit, None)
        self.assertEqual(Resource('property').properties_fwd
                         .get(predicate_id='type').object_lit, None)

class FQDNIDTests(TestCase):
    def assertResult(self, fqdn, id):
        self.assertEqual(id_from_fqdn(fqdn), id)
    def test_simple(self):
        self.assertResult("vh1.csi.cam.ac.uk", "dns--vh1-csi-cam-ac-uk")
    def test_hyphens(self):
        self.assertResult("rnb-vhost07.sys.ds.private.cam.ac.uk",
                              "dns--rnb--vhost07-sys-ds-private-cam-ac-uk")
    def test_multihyphen(self):
        self.assertResult("winnie--csx.grid.private.cam.ac.uk",
                              "dns--winnie----csx-grid-private-cam-ac-uk")
    def test_longish(self):
        self.assertResult("utbs-train-node1.csi.private.cam.ac.uk",
                              "dns--utbs--train--node1-csi-private-cam-ac-uk")
    def test_long(self):
        # This (real) name is just long enough to trip the limit.
        self.assertResult(
            "server-2016-standard-core-template.trinhall.private.cam.ac.uk",
            "2a8a0b4a-926e-5ee6-b2f6-d34e508d59c9")

class ResourceTests(TestCase):
    def test_equality(self):
        r1 = Resource("foo")
        r2 = Resource("foo")
        self.assertTrue(r1 == "foo")
        self.assertTrue("foo" == r1)
        self.assertTrue(r1 == r2)
        self.assertFalse(r1 != "foo")
        self.assertFalse("foo" != r1)
        self.assertFalse(r1 != r2)
    def test_inequality(self):
        r1 = Resource("foo")
        r2 = Resource("bar")
        self.assertFalse(r1 == "bar")
        self.assertFalse("bar" == r1)
        self.assertFalse(r1 == r2)
        self.assertTrue(r1 != "bar")
        self.assertTrue("bar" != r1)
        self.assertTrue(r1 != r2)
    def test_stringify(self):
        Resource("label").save() # So it can be referenced below.
        Resource("incorporates").save() # So it can be referenced below.
        r = Resource("foo")
        self.assertEqual(text_type(r), "<foo>")
        r.save()
        r.addprop("label", "bar")
        self.assertEqual(text_type(r), "bar")
        r1 = Resource("baz")
        r1.save()
        r1.addprop("incorporates", r)
        self.assertEqual(text_type(r), "<baz>:bar")
        r1.addprop("label", "quux")
        self.assertEqual(text_type(r), "quux:bar")
        r1.addprop("incorporates", r1)
        self.assertEqual(text_type(r1), "...:quux")
    def test_createdup(self):
        Resource("foo").save()
        Resource("foo").save()
        self.assertEqual(Resource.objects.filter(pk="foo").count(), 1)
    def test_intern_foreign(self):
        Resource("alias").save()
        r1 = Resource.objects.intern("foreign--thing")
        self.assertNotEqual(r1.id, "foreign--thing")
        r2 = Resource.objects.intern("foreign--thing")
        self.assertEqual(r1, r2)
    def test_addprop(self):
        Resource("label").save()
        r = Resource("foo")
        r.save()
        r.addprop("label", "bar")
        self.assertEqual(r.fwd("label"), ["bar"])
        r.addprop("label", "bar")
        self.assertEqual(r.fwd("label"), ["bar"])
    def test_addprop2(self):
        Resource("property").save()
        Resource("type").save()
        r = Resource("label")
        r.save()
        r.addprop("type", Resource("property"))
        self.assertEqual(r.fwd("type"), [Resource("property")])
        r.addprop("type", Resource("property"))
        self.assertEqual(r.fwd("type"), [Resource("property")])
    def test_delprop(self):
        Resource("label").save()
        r = Resource("foo")
        r.save()
        r.addprop("label", "bar")
        self.assertEqual(r.fwd("label"), ["bar"])
        r.delprop("label", "bar")
        self.assertEqual(r.fwd("label"), [])
    def test_setprop(self):
        Resource("label").save()
        Resource("fqdn").save()
        r = Resource.objects.create()
        r.addprop("label", "bar")
        r.addprop("label", "baz")
        r.addprop("fqdn", "spoo.example")
        r.setprop("label", ["baz", "murfl"])
        self.assertEqual(set(r.fwd("label")), set(["baz", "murfl"]))
        self.assertEqual(set(r.fwd("fqdn")), set(["spoo.example"]))
    def test_listprop(self):
        Resource("label").save()
        Resource("fqdn").save()
        r = Resource.objects.create()
        r.addprop("label", "bar")
        r.addprop("label", "baz")
        r.addprop("fqdn", "spoo.example")
        r.setprop("label", ["baz", "murfl"])
        # <alias> is inferred automatically.
        self.assertEqual(r.listprop(), set(["label", "fqdn", "alias"]))
    def test_merge(self):
        Resource("alias").save()
        Resource("label").save()
        murfl = Resource.objects.create()
        murflid = murfl.id
        spoo = Resource.objects.create()
        spooid = spoo.id
        murfl.addprop("label", "murfl")
        spoo.addprop("label", "spoo")
        murfl.addprop("label", "dup")
        spoo.addprop("label", "dup")
        spoo.merge([murfl])
        self.assertEqual(set(spoo.fwd("label")), set(["murfl", "spoo", "dup"]))
        self.assertEqual(set(spoo.fwd("alias")), set([murflid]))
        with self.assertRaises(Resource.DoesNotExist):
            Resource.objects.get(pk=murflid)
        Resource.objects.get(pk=spooid)
        # Merging into yourself should be a no-op
        spoo.merge([spoo])
        Resource.objects.get(pk=spooid)
    def test_search(self):
        sj.do_import({'@graph': [{'@id': 'foo'},
                                 {'@id': 'bar', 'powers': ['foo',
                                                           {'@id': 'foo'},
                                                           {'@id': 'bar'}]},
                                 {'@id': 'baz'}]})
        foo = Resource.objects.get(id='foo')
        bar = Resource.objects.get(id='bar')
        baz = Resource.objects.get(id='baz')
        self.assertEqual(list(bar.search_transitively('powers',
                                                 lambda x: x.id == 'foo')),
                         [foo])
        self.assertEqual(list(baz.search_transitively('powers',
                                                 lambda x: x.id == 'foo')),
                         [])
    def test_has_type(self):
        sj.do_import({'@graph': [
            { '@id': 'entity', '@type': 'class' },
            { '@id': 'thing', '@type': 'class',
              'subclass-of': {'@id': 'entity' } },
            { '@id': 'box', '@type': 'class',
              'subclass-of': {'@id': 'thing'} },
            { '@id': 'foo', '@type': 'box' },
            { '@id': 'sponge', '@type': 'class',
              'subclass-of': { '@id': 'thing' }}]})
        sj.do_import({'@graph': [
            { '@id': 'entity', '@type': 'class',
              'subclass-of': { '@id': 'thing'}}]})
        foo = Resource.objects.get(id='foo')
        self.assertTrue(foo.has_type('box'))
        self.assertTrue(foo.has_type('thing'))
        self.assertTrue(foo.has_type('entity'))
        self.assertFalse(foo.has_type('sponge'))

class GetIdTests(TestCase):
    def setUp(self):
        sj.do_import([
            { '@id': 'one' },
            { '@id': 'deux', 'alias': 'two' },
            { '@id': 'trois', 'alias': 'three' },
            { '@id': 'drei', 'alias': 'three' }
            ])
    def test_get_id_simple(self):
        one = Resource.objects.get_id('one')
        self.assertEqual(one.id, 'one')
    def test_get_id_alias(self):
        two = Resource.objects.get_id('two')
        self.assertEqual(two.id, 'deux')
    def test_get_id_multiple(self):
        three = Resource.objects.get_id('three')
        self.assertEqual(three.id, 'drei') # first alphabetically
    def test_get_id_none(self):
        with self.assertRaises(Resource.DoesNotExist):
            four = Resource.objects.get_id('four')

class ImportTests(TestCase):
    def test_diaspora_import(self):
        import ucs_besplusplus.importer.diaspora as diaspora
        rack = Resource("rack")
        rack.save()
        rack.addprop("type", Resource("class"))
        box = Resource("box")
        box.save()
        box.addprop("type", Resource("class"))
        contains = Resource("contains")
        contains.save()
        contains.addprop("type", Resource("property"))
        ucsinv = Resource("ucs-inv")
        ucsinv.save()
        ucsinv.addprop("type", Resource("property"))
        label = Resource("label")
        label.save()
        label.addprop("type", Resource("property"))
        diaspora.do_import(StringIO(
            """
            box1:
              to: rnb-c
              inv: 12345
            box2:
              to: rnb-c
              inv: 12346
            box3:
              to: rnb-d
              inv: 12347
            """))
        box1 = Resource.objects.get(properties_fwd__predicate_id="label",
                            properties_fwd__object_lit="box1")
        self.assertEqual(box1.fwd("ucs-inv"), ["12345"])
        
class PropertyTests(TestCase):
    # Low-level tests of the Property object
    def test_clean(self):
        Resource("murfl").save()
        Resource("spoo").save()
        Resource("foo").save()
        spoo = Resource.objects.get(pk="spoo")
        spoo.addprop('type', Resource('property'))
        p = Property(subject_id="murfl", predicate=spoo,
                     object_res_id="foo", object_lit="bar")
        p.full_clean()
        self.assertEqual(p.object_lit, None)
        with self.assertRaises(ValidationError):
            q = Property(subject_id="murfl", predicate_id="spoo")
            q.full_clean()

@override_settings(AUTHENTICATION_BACKENDS=('django.contrib.auth.backends.RemoteUserBackend',))
class DetailViewTests(TestCase):
    def test_redirect(self):
        alias = Resource('alias')
        alias.save()
        spoo = Resource("spoo")
        spoo.save()
        spoo.addprop('alias', "murfl")
        self.client.login(remote_user='test0001')
        response = self.client.get(reverse('resource', args=('murfl',)))
        self.assertEqual(response.status_code, 301)
    def test_link(self):
        alias = Resource('alias')
        alias.save()
        spoo = Resource("spoo")
        spoo.save()
        spoo.addprop('alias', "murfl")
        self.client.login(remote_user='test0001')
        response = self.client.get(reverse('resource', args=('spoo',)))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Link' in response)
