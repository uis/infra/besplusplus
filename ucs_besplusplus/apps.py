from __future__ import unicode_literals

from django.apps import AppConfig

class BesPlusPlusConfig(AppConfig):
    name = 'ucs_besplusplus'
    verbose_name = 'Bes++'
    def ready(self):
        from . import signals
