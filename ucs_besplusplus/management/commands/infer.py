from __future__ import print_function, unicode_literals

from django.core.management.base import BaseCommand, CommandError
import sys

from ...inference import infer_everything_everywhere

class Command(BaseCommand):
    args = ""
    help = "Re-run inference rules over the whole database"

    def handle(self, *args, **options):
        if len(args) != 0:
            raise CommandError("No arguments may be specified")
        infer_everything_everywhere()
