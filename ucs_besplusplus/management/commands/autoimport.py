from __future__ import print_function, unicode_literals

from django.core.management.base import BaseCommand, CommandError
from importlib import import_module
import sys

from ...importer import autoimport_all

class Command(BaseCommand):
    args = ""
    help = "Automatically import data into Bes++ based on database properties"

    def handle(self, *args, **options):
        if len(args) != 0:
            raise CommandError("No arguments may be specified")
        autoimport_all()
                

