from __future__ import print_function, unicode_literals

from django.core.management.base import BaseCommand, CommandError
from importlib import import_module
from django.utils.six import text_type
from argparse import FileType
import sys

class Command(BaseCommand):
    help = "Imports data into Bes++ through the named importer"

    def add_arguments(self, parser):
        parser.add_argument('importer')
        parser.add_argument('file', nargs='?', type=FileType('r'),
                            default=sys.stdin)
        parser.add_argument('--source', type=text_type)

    def handle(self, *args, **options):
        module = import_module('ucs_besplusplus.importer.%s' %
                               (options['importer']))
        if options['source']:
            module.do_import(options['file'], source=options['source'])
        else:
            module.do_import(options['file'])
