-- This file contains additional SQL to be executed after creating tables.

-- Constraints defined here are not required for operation (model code
-- should enforce them itself), but serve to trap errors before they
-- corrupt the database.
ALTER TABLE ucs_besplusplus_property ADD CONSTRAINT single_object 
      CHECK ((object_res_id IS NULL AND object_lit IS NOT NULL) OR
       	     (object_res_id IS NOT NULL AND object_lit IS NULL));
ALTER TABLE ucs_besplusplus_property ADD CONSTRAINT unique_res
      UNIQUE (subject_id, predicate_id, object_res_id);
ALTER TABLE ucs_besplusplus_property ADD CONSTRAINT unique_lit
      UNIQUE (subject_id, predicate_id, object_lit);
