# PowerShell module for interacting with Bes++

function PostProcess-Bes++Resource {
    Param([Parameter(Mandatory = $true, ValueFromPipeline = $true)]
          $Resource)
    process {
        $Resource.PSObject.Properties.Remove('@context')
        foreach ($p in $Resource.PSObject.Properties) {
            if ($p.name -ne "@id" -and $p.value.Count -eq 1) {
	        $p.value = $p.value[0]
            }
        }
        foreach ($p in $Resource."@reverse".PSObject.Properties) {
            if ($p.value.Count -eq 1) {
                $p.value = $p.value[0]
            }
        }
        $Resource
    }
}

function Get-Bes++Resource {
    Param([Parameter(Mandatory = $true, ValueFromPipeline = $true)]
          [String[]] $ResourceID)
    Process {
        $id = $ResourceID
        if ($ResourceID -is [Management.Automation.PSCustomObject]) {
	    $id = $ResourceID."@id"
	}
        $r = Invoke-RestMethod https://besplusplus.uis.cam.ac.uk/++/$id.jsonld
	PostProcess-Bes++Resource $r
	$r
    }
}

function Find-Bes++Resource {
    Param($Search)
    $j = Invoke-RestMethod https://besplusplus.uis.cam.ac.uk/++/!search.jsonld?$Search
    if ($j | Get-Member -name "@graph") {
        $j = $j."@graph"
    }
    $j | PostProcess-Bes++Resource
}
