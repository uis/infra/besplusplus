#! /usr/bin/python
'''
Determines whether the resources returned for a given set of queries are
all located in different distinct locations.

Based on 'diverse' by Ben Harris.
'''

from __future__ import print_function

import requests
import sys

rcache = {}

s = requests.Session()
s.headers = {'Accept': 'application/json'}
s.verify=sys.hexversion >= 0x02070000

def get_resource(id):
    j = s.get("https://besplusplus.uis.cam.ac.uk/++/" + id)
    j.raise_for_status()
    return j.json()

def expand_resource(r):
    r.update(get_resource(r['@id']))

def search_resources(pattern):
    if '.' not in pattern: return [get_resource(pattern)]
    r = s.get("https://besplusplus.uis.cam.ac.uk/++/!search",
              params={'fqdn': pattern})
    r.raise_for_status()
    j = r.json()
    if '@graph' in j:
        return j['@graph']
    return [j]

def walk(r, stoptype='building'):
    while True:
        parents = (r['@reverse'].get('contains', []) +
                r['@reverse'].get('incorporates', []) +
                r['@reverse'].get('hosts-vm', []))
        if len(parents) > 1:
            raise Exception("Too many parents for <%s>" % r['@id'])
        if len(parents) == 0:
            return r
        if stoptype in r['@type']:
            return r
        r = parents[0]
        expand_resource(r)

def describe(r):
    description = u""

    if 'label' in r:
        description += r['label'][0]
    else:
        description += r['@id']

    if '@type' in r:
        description += " [%s]" % ', '.join(r['@type'])

    if 'label' in r:
        description += " (%s)" % r['@id']

    return description
    
def main():
    status = 0	# Exit status.
    ls = set()	# Set of locations.
    output = [] # Lines of human-readable output to return.
    for arg in sys.argv[1:]:
        rs = search_resources(arg)
        # each query should return precisely one resource
        if len(rs) != 1: status = 3
        for r in rs:
            l = walk(r)
            # If this location has already been seen before for
            # a different resource, that indicates co-location.
            if l['@id'] in ls:
                status = 2
            # If the location-type is not the correct type for our
            # desired failure-domain, then report at least UNKNOWN.
            elif ('@type' in l and 'building' not in l['@type']):
                if status == 0:
                    status = 3
           
            ls.add(l['@id'])
            
            if r['@id'] == l['@id']:
                output.append("%s is not recorded as attached to anything." % (describe(r)))
            else:
                output.append("%s is in %s" % (describe(r), describe(l)))

    # If no args were given, give help.
    if len(sys.argv) == 1:
        print('ERROR: You must give a list of FQDNs or resource IDs on the command-line.')
        sys.exit(3)

    # If no resources were returned from the search, give help.
    if len(ls) == 0:
        print('ERROR: No Bes++ resources were returned for the given search terms.')
        sys.exit(3)

    if status == 0:
        print('OK: all resources are located in distinct locations.')
    elif status == 2:
        print('CRITICAL: some resources are improperly co-located.')
    elif status == 3:
        print('UNKNOWN: some resource do not have locations recorded.')

    for line in output:
        print(line)

    sys.exit(status)

# --- 

try:
    main()
except Exception as e:
    # Make sure that unexpected exceptions return an UNKNOWN status
    print('UNKNOWN: %s' % e)
    sys.exit(3)
