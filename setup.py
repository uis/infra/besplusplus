from __future__ import print_function, unicode_literals

from distutils.core import setup

setup(name='ucs-besplusplus',
      packages=['ucs_besplusplus',
                'ucs_besplusplus.directory',
                'ucs_besplusplus.http',
                'ucs_besplusplus.importer',
                'ucs_besplusplus.management',
                'ucs_besplusplus.management.commands',
                'ucs_besplusplus.templatetags',
                'ucs_besplusplus.views'],
      package_data={'ucs_besplusplus':
                    ['templates/*/*.html', 'fixtures/*.yaml',
                     'sql/*.sql',
                     'static/images/interface/*.png',
                     'static/stylesheets/*.css']},
      scripts=['import-agent/dkx2-import-agent', 'clients/check_distinct'],
      )
