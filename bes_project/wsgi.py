from __future__ import print_function, unicode_literals

"""
WSGI config for bes_project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bes_project.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
