from __future__ import print_function, unicode_literals

from django.conf.urls import include, url
from django.conf import settings

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='index')),
    url(r'^\+\+/', include('ucs_besplusplus.urls')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
if ('ucs_besplusplus.auth.BesPlusPlusRavenBackend' in
    settings.AUTHENTICATION_BACKENDS):
    urlpatterns += [url(r'', include('ucamwebauth.urls'))]
else:
    urlpatterns += [url(r'^accounts/', include('django.contrib.auth.urls'))]
