Name: ucs_besplusplus
Version: 0.20170829
Release: 1
Summary: Bes++ Web application
Source: %{name}-%{version}.tar.gz
Group: Unknown
License: None
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildArch: noarch
%define nagios_plugindir /usr/lib/nagios/plugins

Requires: python-Django >= 1.6 python-yaml python-simpleparse python-requests
Requires: campl-apps python-ibisclient django-ucamprojectlight >= 1.2

%Package dkx2-import-agent
Summary: Bes++ import agent for Dominion KX II KVMs
Group: Unknown
Requires: python-mechanize python-yaml

%Package check-distinct
Summary: Nagios plugin for checking server locations in Bes++
Group: System/Monitoring
Requires: python-requests nagios

%Description
This package contains the Bes++ Web application.

%Description dkx2-import-agent
The script in this package is intended to be run on a system with access
to a Raritan Dominion KX II network KVM switch.  It will produce a list
of the CIMs connected to that switch suitable for importing into Bes++.

%Description check-distinct
This package contains a Nagios plugin that checks that a group of
servers (real or virtual) are located in different places according to
Bes++.

%Prep
%setup

%Build
python setup.py build

%Install
python setup.py install -O1 --prefix=%{_prefix} --root="${RPM_BUILD_ROOT}" --record=INSTALLED_FILES
grep -v '^/usr/bin' INSTALLED_FILES > INSTALLED_FILES.MAIN
mkdir -p "${RPM_BUILD_ROOT}"%{nagios_plugindir}
mv "${RPM_BUILD_ROOT}"/usr/bin/check_distinct \
   "${RPM_BUILD_ROOT}"%{nagios_plugindir}/.

%Clean
rm -rf "${RPM_BUILD_ROOT}"

%Files -f INSTALLED_FILES.MAIN
%defattr(-,root,root)

%Files dkx2-import-agent
/usr/bin/dkx2-import-agent

%Files check-distinct
%{nagios_plugindir}/check_distinct
